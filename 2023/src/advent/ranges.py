from __future__ import annotations
from typing import Iterator
from dataclasses import dataclass
from itertools import chain


@dataclass(frozen=True)
class Range:
    start: int
    end: int

    def __iter__(self) -> Iterator[int]:
        return iter((self.start, self.end))

    def __eq__(self, other: Range) -> bool:
        return (self.start == other.start) and (self.end == other.end)

    def __lt__(self, other: Range) -> bool:
        return self.start < other.start

    def __contains__(self, x: int) -> bool:
        return (self.start <= x) and (x <= self.end)

    def intersection(self, other: Range) -> Range | None:
        if self.end < other.start or self.start > other.end:
            return None
        else:
            return Range(
                start=max(self.start, other.start),
                end=min(self.end, other.end),
            )

    def relative_complement(self, other: Range) -> Range | None:
        if self.end < other.start or self.start > other.end:
            return Range(self.start, self.end)
        if self.end <= other.end and self.start >= other.start:
            return None
        if self.start < other.start:
            return Range(self.start, other.start - 1)
        return Range(other.end + 1, self.end)

    def touch(self, other: Range) -> bool:
        return (other.end + 1 == self.start) or (self.end + 1 == other.start)

    def merge(self, other: Range) -> Range:
        return Range(start=min(self.start, other.start), end=max(self.end, other.end))


@dataclass(frozen=True)
class DisjointRange:
    ranges: tuple[Range]

    def overlap(self, other: DisjointRange) -> DisjointRange | None:
        left_ranges = iter(self.ranges)
        right_ranges = iter(other.ranges)


def merge(*ranges: Range) -> list[Range]:
    ranges = sorted(ranges)
    merged: list[Range] = [Range(ranges[0].start, ranges[0].end)]
    for r in ranges[1:]:
        end = merged[-1]
        if end.touch(r) or end.intersection(r) is not None:
            merged[-1] = end.merge(r)
        else:
            merged.append(r)
    return merged


@dataclass(frozen=True)
class RangeMap:
    input_range: Range
    offset: int

    def __lt__(self, other: RangeMap) -> bool:
        return self.input_range < other.input_range

    def map(self, x: int) -> int:
        return x + self.offset if x in self.input_range else x

    def map_range(self, x: Range) -> list[Range]:
        left = x.intersection(
            Range(start=float("-inf"), end=self.input_range.start - 1)
        )
        overlap = self.input_range.intersection(x)
        right = x.intersection(Range(start=self.input_range.end + 1, end=float("inf")))
        mapped = None
        if overlap is not None:
            bounds = sorted(map(self.map, overlap))
            mapped = Range(*list(bounds))
        return merge(*[x for x in (left, mapped, right) if x is not None])

    def map_ranges(self, *ranges: Range) -> list[Range]:
        mapped: list[Range] = []
        for range in ranges:
            mapped.extend(self.map_range(range))
        return merge(*mapped)
