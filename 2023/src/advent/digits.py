import io
import regex

DIGIT_MAP = {
    "one": "1",
    "two": "2",
    "three": "3",
    "four": "4",
    "five": "5",
    "six": "6",
    "seven": "7",
    "eight": "8",
    "nine": "9",
}

DIGITS_RE = "|".join(DIGIT_MAP.keys())


def parse_digits(text: str) -> str:
    raw_digits = regex.findall(r"[0-9]+|" + DIGITS_RE, text, overlapped=True)
    digits = ""
    for raw_digit in raw_digits:
        digit = DIGIT_MAP.get(raw_digit, raw_digit)
        digits += digit
    return digits


def parse_calibration_file(fs: io.IOBase) -> int:
    sum = 0
    for line in fs.readlines():
        digits = parse_digits(line)
        if not len(digits):
            continue
        val = 10 * int(digits[0]) + int(digits[-1])
        sum += val
    return sum
