from __future__ import annotations
from dataclasses import dataclass

import fire

from advent import ranges
from advent.ranges import Range, RangeMap


@dataclass(frozen=True)
class Value:
    label: str
    value: int


@dataclass(frozen=True)
class ValueRange:
    label: str
    range: list[Range]


@dataclass
class Almanac:
    value_maps: dict[str, list[RangeMap]]
    label_maps: dict[str, str]

    def map(self, value: Value):
        dest_label = self.label_maps[value.label]
        for map in self.value_maps[value.label]:
            if value.value in map.input_range:
                return Value(label=dest_label, value=map.map(value.value))
        return Value(label=dest_label, value=value.value)

    def has_map(self, value: Value | ValueRange):
        return value.label in self.label_maps

    def map_range(self, value_range: ValueRange) -> ValueRange:
        dest_label = self.label_maps[value_range.label]
        maps = self.value_maps[value_range.label]
        output: list[Range] = []
        complements: list[Range] = []
        for range_ in value_range.range:
            complement = Range(range_.start, range_.end)
            for map_ in maps:
                intersect = map_.input_range.intersection(range_)
                if complement is not None:
                    complement = complement.relative_complement(map_.input_range)
                if intersect is not None:
                    output.extend(map_.map_range(intersect))
            if complement is not None:
                complements.append(complement)
        return ValueRange(label=dest_label, range=ranges.merge(*output, *complements))


def main(filename: str):
    seeds: list[int] = []
    seed_ranges: list[Range] = []
    maps: dict[str, list[RangeMap]] = {}
    labels: dict[str, str] = {}
    with open(filename, "r") as infile:
        lines = iter(l.strip() for l in infile)
        for line in lines:
            if not line:
                continue
            if line.startswith("seeds:"):
                seed_nums = [int(s) for s in line.split(": ")[1].split()]
                seeds.extend(seed_nums)
                seed_ranges.extend(
                    ranges.merge(
                        *[
                            Range(
                                start=seed_nums[2 * i],
                                end=seed_nums[2 * i] + seed_nums[2 * i + 1] - 1,
                            )
                            for i in range(len(seed_nums) // 2)
                        ]
                    )
                )
            if line.endswith("map:"):
                map_name = line.split()[0]
                source, dest = map_name.split("-to-")
                labels[source] = dest
                maps[source] = set()
                line = next(lines)
                while line:
                    dest_start, source_start, range_len = map(int, line.split())
                    maps[source].add(
                        RangeMap(
                            input_range=Range(
                                start=source_start, end=source_start + range_len - 1
                            ),
                            offset=dest_start - source_start,
                        )
                    )
                    try:
                        line = next(lines)
                    except StopIteration:
                        break
    almanac = Almanac(value_maps=maps, label_maps=labels)
    lowest_location = float("inf")
    for seed in seeds:
        value = Value(label="seed", value=seed)
        print(f"seed: {seed}")
        while almanac.has_map(value):
            value = almanac.map(value)
            print(f"  to {value.label}: {value.value}")
        lowest_location = min(lowest_location, value.value)
    print(f"lowest location (part 1): {lowest_location}")
    seed_range = ValueRange(label="seed", range=seed_ranges)
    print(f"seed: {seed_range}")
    value_range = seed_range
    while almanac.has_map(value_range):
        value_range = almanac.map_range(value_range)
        print(f"  to {value_range.label}: {value_range.range}")
    print(f"lowest location (part 2): {value_range.range[0].start}")


if __name__ == "__main__":
    fire.Fire(main)
