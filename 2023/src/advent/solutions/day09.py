from __future__ import annotations

import multiprocessing

import fire
from operator import itemgetter
from sympy.matrices import Matrix, ones


def get_prev_next_values(seq: Matrix) -> int:
    print(f"Solving: {seq}")
    (n, _) = seq.shape
    X = Matrix(n, n, lambda i, j: (i + 1) ** (j))
    a = X.solve(seq)  # type: Matrix
    next_value = round(sum(a.T * Matrix([(n + 1) ** i for i in range(n)])))
    prev_value = round(sum(a.T * Matrix([0**i for i in range(n)])))
    return prev_value, next_value


def main(filename: str):
    seqs: list[Matrix] = []
    with open(filename, "r") as infile:
        for line in infile:
            if not line.strip():
                continue
            row = Matrix([int(x) for x in line.strip().split()])
            seqs.append(row)
    with multiprocessing.Pool() as pool:
        values = pool.map(get_prev_next_values, seqs)
    for seq, (prev_val, next_val) in zip(seqs, values):
        print(f"{prev_val} ==> {list(seq)} ==> {next_val}")
    print(f"total previous value: {sum(map(itemgetter(0), values))}")
    print(f"total next value: {sum(map(itemgetter(1), values))}")


if __name__ == "__main__":
    fire.Fire(main)
