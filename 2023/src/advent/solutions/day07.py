from __future__ import annotations
from operator import itemgetter

from dataclasses import dataclass, field, replace
from collections import Counter

import fire

SYMBOL_TO_VALUE = {
    "J": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "T": 10,
    "Q": 11,
    "K": 12,
    "A": 13,
}

VALUE_TO_SYMBOL = {v: k for k, v in SYMBOL_TO_VALUE.items()}

TYPES = {
    (5,): 7,
    (4, 1): 6,
    (3, 2): 5,
    (3, 1, 1): 4,
    (2, 2, 1): 3,
    (2, 1, 1, 1): 2,
    (1, 1, 1, 1, 1): 1,
}


@dataclass(frozen=True)
class Card:
    symbol: str
    value: int

    def __lt__(self, other: Card) -> bool:
        return self.value < other.value

    def __eq__(self, other: Card) -> bool:
        return self.value == other.value

    @classmethod
    def from_value(cls, value: int) -> Card:
        return cls(symbol=VALUE_TO_SYMBOL[value], value=value)

    @classmethod
    def from_symbol(cls, symbol: str) -> Card:
        return cls(symbol=symbol, value=SYMBOL_TO_VALUE[symbol])

    @property
    def is_joker(self) -> bool:
        return self.symbol == "J"


@dataclass(frozen=True)
class Hand:
    bid: int
    cards: tuple[Card]

    @property
    def counts(self) -> Counter[Card]:
        return Counter(self.cards)

    def __str__(self) -> str:
        return "".join(c.symbol for c in self.cards)

    def __lt__(self, other: Hand) -> bool:
        counts = tuple(sorted(self.resolved_hand().counts.values(), reverse=True))
        other_counts = tuple(
            sorted(other.resolved_hand().counts.values(), reverse=True)
        )
        if TYPES[counts] != TYPES[other_counts]:
            return TYPES[counts] < TYPES[other_counts]
        for s, o in zip(self.cards, other.cards):
            if s != o:
                return s < o
        return False

    def __eq__(self, other: Hand) -> bool:
        counts = tuple(sorted(self.counts.values(), reverse=True))
        other_counts = tuple(sorted(other.counts.values(), reverse=True))
        if TYPES[counts] != TYPES[other_counts]:
            return False
        for s, o in zip(self.cards, other.cards):
            if s != o:
                return False
        return True

    @classmethod
    def from_cards_str(cls, cards_str: str, bid: int) -> Hand:
        return cls(
            bid=bid,
            cards=tuple(Card.from_symbol(c) for c in cards_str),
        )

    def resolved_hand(self) -> Hand:
        if all(c.symbol == "J" for c in self.cards):
            return Hand.from_cards_str(cards_str="AAAAA", bid=self.bid)
        counts = self.counts
        (most_common,) = max(
            ((c, v) for c, v in counts.items() if not c.is_joker),
            key=itemgetter(1),
        )
        resolved_cards = tuple(most_common if c.is_joker else c for c in self.cards)
        return replace(self, cards=resolved_cards)


def main(filename: str):
    hands: list[Hand] = []
    with open(filename, "r") as infile:
        for line in infile:
            if not line.strip():
                continue
            cards_str, bid_str = line.strip().split(" ")
            hands.append(
                Hand.from_cards_str(
                    cards_str=cards_str,
                    bid=int(bid_str),
                )
            )
    hands.sort()
    total = 0
    for hand_idx, hand in enumerate(hands):
        rank = hand_idx + 1
        total += rank * hand.bid
    print(f"total winnings: {total}")


if __name__ == "__main__":
    fire.Fire(main)
