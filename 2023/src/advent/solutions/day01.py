import fire

from advent.digits import parse_calibration_file


def main(filename: str) -> int:
    with open(filename) as infile:
        return parse_calibration_file(infile)


if __name__ == "__main__":
    fire.Fire(main)
