from __future__ import annotations

from dataclasses import dataclass, replace
from enum import StrEnum

import fire


class Condition(StrEnum):
    UNKNOWN = "?"
    OPERATIONAL = "."
    DAMAGED = "#"


@dataclass(frozen=True)
class ConditionRecord:
    conditions: str
    target: tuple[int, ...]

    def __eq__(self, other: ConditionRecord) -> bool:
        return (
            self.conditions == other.conditions
            and len(self.target) == len(other.target)
            and all(s == o for s, o in zip(self.target, other.target))
        )

    def __str__(self) -> str:
        return f"{self.target}\t{self.conditions}"

    def reduce(self) -> ConditionRecord:
        conditions = self.conditions.strip(Condition.OPERATIONAL)
        blocks = filter(None, conditions.split(Condition.OPERATIONAL))
        conditions = Condition.OPERATIONAL.join(blocks)
        return ConditionRecord(conditions=conditions, target=self.target)

    def trim_end(self) -> ConditionRecord:
        """Must be applied to a reduced record."""
        if not self.target:
            return self
        blocks = self.conditions.split(Condition.OPERATIONAL)
        end = blocks[-1]
        record = self
        if Condition.UNKNOWN not in end and len(end) == self.target[-1]:
            record = ConditionRecord(
                conditions=Condition.OPERATIONAL.join(blocks[:-1]),
                target=self.target[:-1],
            )
        return record

    def trim_start(self) -> ConditionRecord:
        """Must be applied to a reduced record."""
        if not self.target:
            return self
        blocks = self.conditions.split(Condition.OPERATIONAL)
        start = blocks[0]
        record = self
        if Condition.UNKNOWN not in start and len(start) == self.target[0]:
            record = ConditionRecord(
                conditions=Condition.OPERATIONAL.join(blocks[1:]),
                target=self.target[1:],
            )
        return record

    def trim(self) -> ConditionRecord:
        old = self
        trimmed = self.trim_start().trim_end()
        while old != trimmed:
            old = trimmed
            trimmed = trimmed.trim_start().trim_end()
        return trimmed

    @property
    def is_possible(self) -> bool:
        if not self.target and not self.conditions:
            return True
        if (not self.target and self.conditions) or (
            self.target and not self.conditions
        ):
            return False
        blocks = self.conditions.split(Condition.OPERATIONAL)
        start = blocks[0]
        end = blocks[-1]
        return (Condition.UNKNOWN in start or len(start) == self.target[0]) and (
            Condition.UNKNOWN in end or len(end) == self.target[-1]
        )


def find_arrangements_rec(record: ConditionRecord, count: int) -> int:
    if not record.target and (
        not record.conditions or Condition.DAMAGED not in record.conditions
    ):
        return count
    if not record.is_possible:
        return 0
    cond1 = record.conditions.replace(Condition.UNKNOWN, Condition.DAMAGED, 1)
    cond2 = record.conditions.replace(Condition.UNKNOWN, Condition.OPERATIONAL, 1)
    record1 = replace(record, conditions=cond1).reduce().trim()
    record2 = replace(record, conditions=cond2).reduce().trim()
    count1 = find_arrangements_rec(record1, count)
    count2 = find_arrangements_rec(record2, count)
    return count * (count1 + count2)


def find_arrangements(record: ConditionRecord) -> int:
    record = record.reduce()
    record = record.trim()
    if record.is_possible:
        count = find_arrangements_rec(record=record, count=1)
    else:
        return 0
    return count


def main(filename: str):
    records: list[ConditionRecord] = []
    with open(filename, "r", encoding="utf-8") as infile:
        print("raw records:")
        for line in (l.strip() for l in infile):
            conditions_str, target_str = line.split(" ")
            target = tuple(int(t) for t in target_str.split(","))
            record = ConditionRecord(conditions=conditions_str, target=target)
            print(f"  {record}")
            records.append(record)
    print("finding arrangements")
    total = 0
    for record in records:
        total += find_arrangements(record)
    print(total)


if __name__ == "__main__":
    fire.Fire(main)
