import fire
import numpy as np


def main(filename: str):
    with open(filename, "r") as infile:
        lines = [line.strip() for line in infile.readlines()]
    sum_ = 0
    weights = np.array([1] * len(lines))
    for card_idx, line in enumerate(lines):
        weight = weights[card_idx]
        print(f"Processing card {card_idx+1}.")
        nums = line.split(":")[1].strip()
        left, right = nums.split("|")
        winning = {int(l) for l in left.strip().split(" ") if l}
        actual = {int(r) for r in right.strip().split(" ") if r}
        raw_score = len(winning) - len(winning - actual)
        weights[(card_idx + 1) : (card_idx + raw_score + 1)] += weight
        if raw_score > 0:
            sum_ += weight * 2 ** (raw_score - 1)
            print(f"Weight {weight}")
    print(sum(weights))


if __name__ == "__main__":
    fire.Fire(main)
