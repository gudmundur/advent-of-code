from __future__ import annotations
from math import prod
from dataclasses import dataclass
from itertools import cycle

import fire
from sympy import sieve


@dataclass
class Node:
    label: str
    left: Node | None = None
    right: Node | None = None

    def __eq__(self, other: Node):
        return self.label == other.label

    def __str__(self):
        return f"{self.label} = ({self.left.label}, {self.right.label})"


def cycle_detection(directions: list[str], node: Node) -> tuple[int]:
    visited: set[(int, str)] = set()
    steps = 0
    termini: list[int] = []
    for dir_idx, dir in cycle(enumerate(directions)):
        if (dir_idx, node.label) in visited:
            break
        visited.add((dir_idx, node.label))
        match dir:
            case "R":
                node = node.right
            case "L":
                node = node.left
        steps += 1
        if node.label.endswith("Z"):
            termini += [steps]
    return tuple(termini)


def get_factors(n: int, primes: list[int]):
    return [p for p in primes if n % p == 0]


def main(filename: str):
    node_constructors: list[tuple[str, int, int]] = []
    with open(filename, "r") as infile:
        directions = [d for d in next(infile).strip()]
        for line in infile:
            if not line.strip():
                continue
            label, children = line.strip().split(" = ")
            left, right = children.strip("()").split(", ")
            node_constructors.append((label, left, right))
    node_map: dict[str, Node] = {}
    for label, _, _ in node_constructors:
        node_map[label] = Node(label=label)
    for label, left, right in node_constructors:
        node = node_map[label]
        node.left = node_map[left]
        node.right = node_map[right]
    nodes = [n for l, n in node_map.items() if l.endswith("A")]
    prime_factors: set[int] = set()
    for node in nodes:
        loop = cycle_detection(directions=directions, node=node)
        print(loop)
        for l in loop:
            sieve.extend_to_no(l)
            for p in get_factors(l, sieve._list.tolist()):
                prime_factors.add(p)
    print(prod(prime_factors))


if __name__ == "__main__":
    fire.Fire(main)
