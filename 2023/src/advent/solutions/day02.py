from __future__ import annotations
from collections import Counter
import dataclasses
from operator import mul
from functools import reduce

import fire
import regex

GAME_RE: regex.Pattern = regex.compile(r"^Game \d+: (?P<game>.*)$")
REFERENCE_SET = Counter({"red": 12, "green": 13, "blue": 14})


@dataclasses.dataclass
class Game:
    id: int
    sets: list[Counter[str]]

    def is_possible(self, other: Counter[str]) -> bool:
        for set in self.sets:
            diff = other.copy()
            diff.subtract(set)
            if any(c < 0 for c in diff.values()):
                return False
        return True

    def minimal_set(self) -> Counter[str]:
        min_set = Counter()
        for set in self.sets:
            for color, count in set.items():
                min_set[color] = max(min_set[color], count)
        return min_set

    @classmethod
    def from_str(cls, id: int, text: str) -> Game:
        sets_str = text.split(";")
        sets = []
        for set_str in sets_str:
            cubes_str = set_str.strip().split(",")
            set_dict = {}
            for cube_str in cubes_str:
                count_str, color = cube_str.strip().split()
                count = int(count_str)
                set_dict[color] = count
            sets.append(Counter(set_dict))
        return cls(id, sets)


def power(counter: Counter[str]) -> int:
    return reduce(mul, counter.values())


def main(filename: str) -> int:
    games: list[Game] = []
    with open(filename) as infile:
        for line_number, line in enumerate(infile):
            game_number: int = 1 + line_number
            game_str = GAME_RE.match(line).group("game")
            games.append(Game.from_str(game_number, game_str))
    sum_ids = 0
    for game in games:
        if game.is_possible(REFERENCE_SET):
            sum_ids += game.id
    print(f"Sum of possible IDs: {sum_ids}")
    sum_powers = 0
    for game in games:
        sum_powers += power(game.minimal_set())
    print(f"Sum of powers: {sum_powers}")


if __name__ == "__main__":
    fire.Fire(main)
