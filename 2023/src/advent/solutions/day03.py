import dataclasses
import operator

import fire
import regex


def span_contains(
    bottom_left: (int, int), top_right: (int, int), to_test: (int, int)
) -> bool:
    return (to_test[0] >= bottom_left[0] and to_test[0] <= top_right[0]) and (
        to_test[1] >= bottom_left[1] and to_test[1] <= top_right[1]
    )


@dataclasses.dataclass
class PartNumber:
    value: int
    row_idx: int
    span: (int, int)

    def is_adjacent_to(self, other: (int, int)) -> bool:
        left, right = self.span
        row_idx = self.row_idx
        bottom_left = (left - 1, row_idx - 1)
        top_right = (right, row_idx + 1)
        return span_contains(bottom_left, top_right, other)


@dataclasses.dataclass
class Symbol:
    value: str
    location: (int, int)


@dataclasses.dataclass
class Gear:
    gear_ratio: int


@dataclasses.dataclass
class Schematic:
    data: list[str]
    numbers: list[PartNumber] = dataclasses.field(init=False)
    symbols: list[Symbol] = dataclasses.field(init=False)

    def __post_init__(self):
        self.numbers = []
        for row_idx, row in enumerate(self.data):
            number_matches: list[regex.Match] = regex.finditer(r"[0-9]+", row)
            for match in number_matches:
                number = int(match.group(0))
                left, right = match.span()
                self.numbers.append(
                    PartNumber(value=number, row_idx=row_idx, span=match.span())
                )
                span_size = right - left
                row = self.data[row_idx]
                self.data[row_idx] = row[:left] + span_size * "." + row[right:]
        self.symbols = []
        for row_idx, row in enumerate(self.data):
            symbol_matches: list[regex.Match] = regex.finditer(r"[^.]", row)
            for match in symbol_matches:
                self.symbols.append(
                    Symbol(
                        value=match.group(0),
                        location=(match.start(), row_idx),
                    )
                )

    def get_valid_numbers(self) -> list[int]:
        numbers: list[int] = []
        for number in self.numbers:
            for symbol in self.symbols:
                if number.is_adjacent_to(symbol.location):
                    numbers.append(number.value)
                    break
        return numbers

    def get_gears(self) -> list[Gear]:
        gears: list[Gear] = []
        for symbol in (s for s in self.symbols if s.value == "*"):
            adjacent_numbers = [
                n for n in self.numbers if n.is_adjacent_to(symbol.location)
            ]
            if len(adjacent_numbers) != 2:
                continue
            gears.append(
                Gear(gear_ratio=adjacent_numbers[0].value * adjacent_numbers[1].value)
            )
        return gears


def main(filename: str) -> None:
    with open(filename) as infile:
        lines = [line.strip() for line in infile.readlines()]
    schematic = Schematic(lines)
    valid_nums = schematic.get_valid_numbers()
    print(sum(valid_nums))
    print(sum(map(operator.attrgetter("gear_ratio"), schematic.get_gears())))


if __name__ == "__main__":
    fire.Fire(main)
