from __future__ import annotations
from dataclasses import dataclass
from enum import StrEnum

import fire
import numpy as np
import numpy.typing as npt

junction_map: dict[str, list[tuple]] = {
    "|": [(1, 0), (-1, 0)],
    "-": [(0, 1), (0, -1)],
    "L": [(-1, 0), (0, 1)],
    "J": [(-1, 0), (0, -1)],
    "7": [(1, 0), (0, -1)],
    "F": [(1, 0), (0, 1)],
}


class PipeType(StrEnum):
    NS = "|"
    EW = "-"
    NE = "L"
    NW = "J"
    SW = "7"
    SE = "F"
    GROUND = "."
    START = "S"


@dataclass
class Node:
    id: tuple[int, int]
    neighs: set[Node]

    def __hash__(self) -> int:
        return hash(self.id)

    def __str__(self) -> str:
        return f"{self.id} => {' '.join(str(n.id) for n in self.neighs)}"

    def __eq__(self, other: Node) -> bool:
        return self.id == other.id


def read_pipe_map(filename: str) -> npt.NDArray[np.str_]:
    with open(filename, "r") as infile:
        lines = infile.readlines()
        chars = [list(line.strip()) for line in lines]
        pipe_map = np.array(chars, dtype=np.dtype(np.str_))
    return pipe_map


def print_str_array(arr: npt.NDArray[np.str_]):
    print(*["".join(row) for row in arr], sep="\n")


def get_num_inside_nodes(
    in_pipe_map: npt.NDArray[np.str_],
    nodes: dict[tuple[int], Node],
    path_nodes: set[Node],
    start_node: Node,
):
    labels = "+*"
    pipe_map = np.full_like(in_pipe_map, fill_value=".")
    for node in path_nodes:
        pipe_map[node.id] = in_pipe_map[node.id]
    print_str_array(pipe_map)
    print()
    height, width = pipe_map.shape
    # arbitrarily pick a neighbor to start
    prev_node = start_node
    node = next(iter(start_node.neighs))
    while node != start_node:
        next_node = next(n for n in node.neighs if n != prev_node)
        dy1, dx1 = tuple(np.array(node.id) - np.array(prev_node.id))
        dy2, dx2 = tuple(np.array(next_node.id) - np.array(node.id))
        norm1 = np.array((-dx1, dy1))
        norm2 = np.array((-dx2, dy2))
        for norm in (norm1, norm2):
            for label, n in zip(labels, (norm, -norm)):
                loc_y, loc_x = np.array(node.id) + n
                if loc_y >= 0 and loc_y < height and loc_x >= 0 and loc_x < width:
                    if pipe_map[loc_y, loc_x] == ".":
                        pipe_map[loc_y, loc_x] = label
        prev_node = node
        node = next_node
    print_str_array(pipe_map)
    print()
    grid_y, grid_x = np.indices(pipe_map.shape)
    while "." in pipe_map:
        unmarked_mask = pipe_map == "."
        # check neighbors
        unmarked_y, unmarked_x = grid_y[unmarked_mask], grid_x[unmarked_mask]
        for y, x in zip(unmarked_y.ravel(), unmarked_x.ravel()):
            neighs = [(y + 1, x), (y - 1, x), (y, x + 1), (y, x - 1)]
            for neigh_y, neigh_x in neighs:
                if (
                    neigh_y >= 0
                    and neigh_y < height
                    and neigh_x >= 0
                    and neigh_x < width
                ):
                    neigh = pipe_map[neigh_y, neigh_x]
                    if neigh != "." and neigh not in junction_map:
                        pipe_map[y, x] = neigh
                        break
                    if neigh == ".":
                        continue
                    neigh_node = nodes[neigh_y, neigh_x]
                    print(neigh_node)

        print_str_array(pipe_map)
        print()
    print_str_array(pipe_map)
    print()
    # check perimeter to see which label is the outside
    top = pipe_map[0, :].ravel()
    left = pipe_map[:, 0].ravel()
    bottom = pipe_map[height - 1, :].ravel()
    right = pipe_map[:, width - 1].ravel()
    outside = None
    for perim in [top, left, bottom, right]:
        labels = perim[(perim == "+") | (perim == "*")]
        if labels.size > 0:
            outside = next(iter(labels))
            break
        else:
            continue
    if outside is None:
        return np.sum((pipe_map == "*") | (pipe_map == "+"))
    return np.sum((pipe_map != outside) & ((pipe_map == "*") | (pipe_map == "+")))


def main(filename: str):
    pipe_map = read_pipe_map(filename)
    height, width = pipe_map.shape
    grid_y, grid_x = np.indices(pipe_map.shape)
    nodes: dict[tuple[int], Node] = {}
    path_nodes: set[Node] = set()
    # create all the nodes first
    for y_idx, x_idx in zip(grid_y.ravel(), grid_x.ravel()):
        junction_char = pipe_map[(y_idx, x_idx)]
        if junction_char == PipeType.GROUND:
            continue
        node = Node(id=(y_idx, x_idx), neighs=set())
        nodes[(y_idx, x_idx)] = node
    # add neighbors
    for (y_idx, x_idx), node in nodes.items():
        junction_char = pipe_map[(y_idx, x_idx)]
        if junction_char == PipeType.START:
            continue
        for offset in junction_map[junction_char]:
            offset_y, offset_x = offset
            neigh_idx = y_idx + offset_y, x_idx + offset_x
            neigh_y, neigh_x = neigh_idx
            if neigh_y >= 0 and neigh_y < height and neigh_x >= 0 and neigh_x < width:
                neigh_char = pipe_map[neigh_y, neigh_x]
                if neigh_char == PipeType.GROUND:
                    continue
                neigh = nodes[(neigh_y, neigh_x)]
                node.neighs.add(neigh)
    # remove neighbors that are not reciprocal
    for node in nodes.values():
        to_remove: set[Node] = set()
        for neigh in node.neighs:
            neigh_char = pipe_map[neigh.id]
            if neigh_char == PipeType.START:
                neigh.neighs.add(node)
            if node not in neigh.neighs:
                to_remove.add(neigh)
        node.neighs -= to_remove
    path_map = np.full_like(pipe_map, ".")
    for loc in [l for l, n in nodes.items() if len(n.neighs) >= 2]:
        path_map[loc] = "X"
    print(*["".join(row) for row in path_map], sep="\n")
    start_mask = pipe_map == "S"
    start_idx = tuple([grid_y[start_mask][0], grid_x[start_mask][0]])
    start_node = nodes[start_idx]
    dist_map = np.full_like(pipe_map, -1, dtype=np.dtype(np.int64))
    dist_map[start_idx] = 0
    path_nodes.add(start_node)
    for neigh in start_node.neighs:
        dist = 0
        node = neigh
        prev_node = start_node
        while node != start_node:
            path_nodes.add(node)
            dist += 1
            dist_map[node.id] = (
                dist if dist_map[node.id] == -1 else min(dist, dist_map[node.id])
            )
            next_node = next(n for n in node.neighs if n != prev_node)
            prev_node = node
            node = next_node
    print(dist_map)
    print(max(dist_map.ravel()))

    print(get_num_inside_nodes(pipe_map, nodes, path_nodes, start_node))


if __name__ == "__main__":
    fire.Fire(main)
