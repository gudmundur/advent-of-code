#ifndef SPLINE_HH
#define SPLINE_HH

#include <vector>

#include "point.hh"

class Spline
{
private:
    std::vector<Point> vertices;

public:
    Spline(const Spline &) = delete;
    Spline(Spline &&) = default;

    Spline &operator=(const Spline &) = delete;
    Spline &operator=(Spline &&) = default;

    Spline() {}

    const std::vector<Point> &Vertices() const
    {
        return vertices;
    }

    void Clear()
    {
        vertices.clear();
    }

    void PushBack(Point &&point)
    {
        vertices.push_back(std::move(point));
    }

    void PushBack(const Point &point)
    {
        vertices.push_back(point);
    }

    Spline BoundingBox(long margin = 0) const
    {
        Spline box;
        long right = INT64_MIN, top = INT64_MIN;
        long left = INT64_MAX, bottom = INT64_MAX;
        for (auto &vertex : vertices)
        {
            auto &[x, y] = vertex.Coordinate();
            right = std::max(right, x);
            top = std::max(top, y);
            left = std::min(left, x);
            bottom = std::min(bottom, y);
        }
        box.PushBack({left - margin, bottom - margin});
        box.PushBack({left - margin, top + margin});
        box.PushBack({right + margin, top + margin});
        box.PushBack({right + margin, bottom - margin});
        return box;
    }

    Spline Shift(const Point &point) const
    {
        Spline new_spline;
        auto &[x_shift, y_shift] = point.Coordinate();
        for (auto &v : vertices)
        {
            auto &[x, y] = v.Coordinate();
            new_spline.PushBack({x - x_shift, y - y_shift});
        }
        return new_spline;
    }

    void Combine(const Spline &rhs)
    {
        for (auto &v : rhs.vertices)
        {
            this->PushBack(Point(v.X(), v.Y()));
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const Spline &rhs)
    {
        os << "{";
        if (!rhs.vertices.empty())
        {
            os << rhs.vertices[0];
        }
        for (size_t i = 1; i < rhs.vertices.size(); ++i)
        {
            os << " -> " << rhs.vertices[i];
        }
        os << "}";
        return os;
    }
};

#endif
