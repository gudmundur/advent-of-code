#ifndef MESSAGE_MARKER_HH
#define MESSAGE_MARKER_HH

#include <cstddef>
#include <deque>
#include <unordered_map>

class MessageMarker
{
private:
    size_t n_processed = 0;
    size_t marker_size;
    std::deque<char> stream;
    std::unordered_map<char, int> marker;

public:
    MessageMarker(const MessageMarker &) = delete;            // copy constructor
    MessageMarker &operator=(const MessageMarker &) = delete; // copy assignment

    MessageMarker(size_t marker_size) : marker_size(marker_size) {}

    void Push(const char &c)
    {
        ++n_processed;
        // add character
        stream.push_back(c);
        if (!marker.contains(c))
        {
            marker[c] = 0;
        }
        ++marker[c];
        if (stream.size() <= marker_size)
        {
            // don't need to remove character
            return;
        }
        // remove character
        char &exiting = stream.front();
        --marker[exiting];
        if (marker[exiting] == 0)
        {
            marker.erase(exiting);
        }
        stream.pop_front();
    }

    bool MarkerFound() const
    {
        return marker.size() == marker_size;
    }

    const size_t &GetNProcessed() const
    {
        return n_processed;
    }
};

#endif