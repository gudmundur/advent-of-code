#ifndef GRID_HH
#define GRID_HH

#include <cstddef>
#include <iostream>
#include <tuple>
#include <vector>

#include "point.hh"

typedef std::pair<size_t, size_t> index_t;

std::ostream &operator<<(std::ostream &os, const index_t &rhs)
{
    os << "(" << rhs.first << "," << rhs.second << ")";
    return os;
}

class Grid
{
private:
    std::vector<char> data;
    size_t n_cols;
    size_t n_rows;

public:
    Grid(const Grid &) = delete;
    Grid(Grid &&) = default;

    Grid &operator=(const Grid &) = delete;
    Grid &operator=(Grid &&) = default;

    Grid() : n_cols(0), n_rows(0) {}

    Grid(std::vector<char> &&data,
         size_t n_cols,
         size_t n_rows) : data(std::move(data)),
                          n_cols(n_cols),
                          n_rows(n_rows) {}

    const std::vector<char> &Data() const
    {
        return data;
    }

    const size_t &NRows() const
    {
        return n_rows;
    }

    const size_t &NCols() const
    {
        return n_cols;
    }

    char &operator()(const index_t &idx)
    {
        return data[IndexToSize(idx)];
    }

    char &operator()(const Point &point)
    {
        return this->operator()(point.Coordinate());
    }

    char &operator()(const size_t &row, const size_t &col)
    {
        return this->operator()(index_t{row, col});
    }

    const char &at(const size_t &row, const size_t &col) const
    {
        return data.at(IndexToSize({row, col}));
    }

    size_t IndexToSize(const index_t &pair) const
    {
        auto &[row, col] = pair;
        return col + row * n_cols;
    };

    index_t SizeToIndex(const size_t &idx) const
    {
        return index_t(idx / n_cols, idx % n_cols);
    }

    static Grid FromInput(std::istream &input)
    {
        std::vector<char> grid;
        char buffer;
        size_t row = 0;
        size_t col = 0;
        while (input.get(buffer))
        {
            if (buffer == '\n')
            {
                if (input.peek() != EOF)
                {
                    col = 0;
                }
                ++row;
                continue;
            }
            grid.push_back(buffer);
            ++col;
        }
        return Grid(std::move(grid),
                    row,
                    col);
    }

    friend std::ostream &operator<<(std::ostream &os, const Grid &grid)
    {
        for (size_t row = 0; row < grid.n_rows; ++row)
        {
            for (size_t col = 0; col < grid.n_cols; ++col)
            {
                os << grid.data[grid.IndexToSize({row, col})];
            }
            os << std::endl;
        }
        return os;
    }
};

#endif
