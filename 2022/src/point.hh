#ifndef POINT_HH
#define POINT_HH

#include <cmath>
#include <tuple>
#include <unordered_set>
#include <iostream>

template <class T>
int sgn(T val)
{
    return val < 0 ? -1 : val > 0;
}

enum Direction
{
    UP,
    LEFT,
    DOWN,
    RIGHT
};

class Point
{
private:
    std::pair<long, long> coordinate;

public:
    Point(const Point &) = default;            // copy constructor
    Point &operator=(const Point &) = default; // copy assignment
    Point(Point &&) = default;                 // move constructor
    Point &operator=(Point &&) = default;      // move assignment

    Point(std::pair<long, long> &&coordinate) : coordinate(coordinate) {}
    Point(long x, long y) : Point(std::pair<long, long>(x, y)) {}
    Point() : Point(0l, 0l) {}

    const long &X() const
    {
        return coordinate.first;
    }

    const long &Y() const
    {
        return coordinate.second;
    }

    const std::pair<long, long> &Coordinate() const
    {
        return coordinate;
    }

    Point &Move(const Direction &dir)
    {
        switch (dir)
        {
        case UP:
            ++coordinate.second;
            break;
        case DOWN:
            --coordinate.second;
            break;
        case LEFT:
            --coordinate.first;
            break;
        case RIGHT:
            ++coordinate.first;
            break;
        default:
            break;
        }
        return *this;
    }

    Point Direction() const
    {
        return Point(sgn(X()), sgn(Y()));
    }

    Point operator-() const
    {
        return Point(-this->X(), -this->Y());
    }

    Point operator+(const Point &rhs) const
    {
        return Point(this->X() + rhs.X(), this->Y() + rhs.Y());
    }

    Point operator-(const Point &rhs) const
    {
        return *this + (-rhs);
    }

    Point &operator+=(const Point &rhs)
    {
        coordinate.first += rhs.X();
        coordinate.second += rhs.Y();
        return *this;
    }
    Point &operator-=(const Point &rhs)
    {
        coordinate.first -= rhs.X();
        coordinate.second -= rhs.Y();
        return *this;
    };

    bool operator==(const Point &rhs) const
    {
        return (this->X() == rhs.X()) && (this->Y() == rhs.Y());
    }

    friend std::ostream &operator<<(std::ostream &os, const Point &rhs)
    {
        os << "(" << rhs.X() << "," << rhs.Y() << ")";
        return os;
    }
};

template <long p>
double norm(const Point &point)
{
    return std::pow(std::pow(std::abs(point.X()), p) + std::pow(std::abs(point.Y()), p),
                    1 / ((double)p));
};

template <>
double norm<-1>(const Point &point)
// infinity norm
{
    return std::max(std::abs(point.X()), std::abs(point.Y()));
};

template <class T>
inline void hash_combine(std::size_t &seed, const T &v)
{
    // see https://www.boost.org/doc/libs/1_35_0/doc/html/boost/hash_combine_id241013.html
    std::hash<T> hasher;
    seed ^= hasher(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
}

namespace std
{
    template <>
    struct hash<Point>
    {
        size_t operator()(const Point &point) const
        {
            size_t hash = std::hash<long>()(point.X());
            hash_combine(hash, point.Y());
            return hash;
        }
    };
}

#endif
