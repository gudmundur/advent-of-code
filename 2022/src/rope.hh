#ifndef ROPE_HH
#define ROPE_HH

#include <vector>
#include <unordered_set>

#include "point.hh"

class Rope
{
private:
    std::vector<Point> nodes;
    std::unordered_set<Point> tail_trail;

public:
    Rope(const Rope &) = delete;            // copy constructor
    Rope &operator=(const Rope &) = delete; // copy assignment

    Rope(size_t size)
    {
        for (size_t i = 0; i < size; ++i)
        {
            nodes.emplace_back(0ul, 0ul);
        }
        tail_trail.emplace(0ul, 0ul);
    };

    const Point &Tail() const
    {
        return nodes.back();
    }

    const Point &Head() const
    {
        return nodes.front();
    }

    Rope &MoveHead(const Direction &dir)
    {
        nodes[0].Move(dir);
        for (size_t tail_idx = 1; tail_idx < nodes.size(); ++tail_idx)
        {
            Point &head = nodes[tail_idx - 1];
            Point &tail = nodes[tail_idx];
            Point diff = head - tail;
            // check if we need to move at all
            if (norm<1>(diff) <= 1)
                return *this;
            tail += diff.Direction();
        }
        tail_trail.emplace(Tail().X(), Tail().Y());
        return *this;
    }

    const std::unordered_set<Point> &TailTrail() const
    {
        return tail_trail;
    }
};

#endif
