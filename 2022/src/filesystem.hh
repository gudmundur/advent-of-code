#ifndef FILEYSTEM_HH
#define FILESYSTEM_HH

#include <cstddef>
#include <string>
#include <map>
#include <iostream>

enum FileType
{
    FILE_TYPE,
    DIRECTORY_TYPE
};

class FileSystemObject
{
private:
    std::string name;
    mutable std::map<std::string, FileSystemObject> contents;
    std::size_t size;
    FileSystemObject *parent;
    FileType type;

    FileSystemObject(FileSystemObject *parent, std::string name, size_t size, FileType type)
        : parent(parent), name(name), size(size), type(type)
    {
    }

public:
    FileSystemObject(const FileSystemObject &) = delete;            // copy constructor
    FileSystemObject &operator=(const FileSystemObject &) = delete; // copy assignment
    FileSystemObject(FileSystemObject &&rhs) = default;             // move constructor
    FileSystemObject &operator=(FileSystemObject &&) = default;     // move assignemnt

    static FileSystemObject NewFile(FileSystemObject *parent, std::string name, size_t size)
    {
        return FileSystemObject(parent, name, size, FILE_TYPE);
    }

    static FileSystemObject NewDirectory(FileSystemObject *parent, std::string name)
    {
        return FileSystemObject(parent, name, 0, DIRECTORY_TYPE);
    }

    static FileSystemObject CreateRoot()
    {

        return FileSystemObject(nullptr, "/", 0, DIRECTORY_TYPE);
    }

    const std::string &Name() const
    {
        return name;
    }

    const size_t &Size() const
    {
        return size;
    }

    const FileType &Type() const
    {
        return type;
    }

    bool IsFile() const
    {
        return type == FILE_TYPE;
    }

    bool IsDirectory() const
    {
        return type == DIRECTORY_TYPE;
    }

    bool IsRoot() const
    {
        return parent == nullptr;
    }

    FileSystemObject &Parent()
    {
        if (parent == nullptr)
        {
            return *this;
        }
        else
        {
            return *parent;
        }
    }

    size_t TotalSize() const
    {
        if (Type() == FILE_TYPE)
        {
            return Size();
        }
        size_t sum = 0;
        for (auto &child : Contents())
        {
            sum += child.second.TotalSize();
        }
        return sum;
    }

    FileSystemObject &CreateSubdirectory(std::string name)
    {
        contents.emplace(std::piecewise_construct,
                         std::tuple{name},
                         std::tuple{FileSystemObject::NewDirectory(this, name)});
        return contents.at(name);
    }

    FileSystemObject &CreateFile(std::string name, size_t size)
    {
        contents.emplace(std::piecewise_construct,
                         std::tuple{name},
                         std::tuple{FileSystemObject::NewFile(this, name, size)});
        return contents.at(name);
    }

    std::map<std::string, FileSystemObject> &Contents() const
    {
        return contents;
    }

    bool operator<(const FileSystemObject &rhs) const
    {
        return this->Name() < rhs.Name();
    }

    friend std::ostream &operator<<(std::ostream &os, const FileSystemObject &rhs)
    {
        os << rhs.Name() << " (";
        switch (rhs.Type())
        {
        case FILE_TYPE:
            os << "file, size=" << rhs.Size();
            break;
        case DIRECTORY_TYPE:
            os << "dir";
            break;
        default:
            os << "unknown";
            break;
        }
        os << ")";
        return os;
    }
};

namespace std
{
    template <>
    struct hash<FileSystemObject>
    {
        size_t operator()(const FileSystemObject &obj) const
        {
            return std::hash<std::string>()(obj.Name());
        }
    };
}

class FileSystem
{
private:
    FileSystemObject root = FileSystemObject::CreateRoot();
    FileSystemObject *cwd = &root;

    std::ostream &RecStreamHelper(std::ostream &os, const FileSystemObject &obj, size_t indent) const
    {
        for (size_t i = 0; i < indent; ++i)
        {
            os << " ";
        }
        os << " - " << obj;
        for (auto &child : obj.Contents())
        {
            os << std::endl;
            RecStreamHelper(os, child.second, indent + 2);
        }
        return os;
    }

public:
    FileSystem(const FileSystem &) = delete;            // copy constructor
    FileSystem &operator=(const FileSystem &) = delete; // copy assignment

    FileSystem()
    {
    }

    FileSystemObject &Root()
    {
        return root;
    }

    FileSystemObject &Cwd()
    {
        return *cwd;
    }

    FileSystem &ChangeDir(std::string name)
    {
        if (name == ".")
        { // do nothing
        }
        else if (name == "..")
        {
            cwd = &cwd->Parent();
        }
        else if (name == "/")
        {
            cwd = &root;
        }
        else
        {
            cwd = const_cast<FileSystemObject *>(&cwd->Contents().at(name));
        }
        return *this;
    }

    friend std::ostream &operator<<(std::ostream &os, const FileSystem &rhs)
    {
        size_t indent = 0;
        return rhs.RecStreamHelper(os, rhs.root, indent);
    }
};

#endif
