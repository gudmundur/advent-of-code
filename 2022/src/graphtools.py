from typing import Protocol, Optional
from operator import itemgetter


class Node(Protocol):
    label: str
    neighbors: dict[str, int]


class Graph(Protocol):
    def node_from_label(self, label: str) -> Node:
        ...


class Dijkstra:
    def __init__(self, graph: Graph, start: Node, end: Optional[Node] = None):
        self.graph = graph
        self.start = start
        self.end = end
        self.distances = {start.label: 0}  # type: dict[str, int]
        self.visited = set()  # type: set[str]
        self.fringe = [(start.label, 0)]
        self.sort_fringe()

    def sort_fringe(self):
        self.fringe.sort(key=itemgetter(1), reverse=True)

    def __iter__(self):
        return self

    def __next__(self):
        if not self.fringe:
            raise StopIteration()
        curr_label, curr_dist = self.fringe.pop()
        current = self.graph.node_from_label(curr_label)
        if self.end is not None and current == self.end:
            raise StopIteration()
        for neigh_label, neigh_dist in current.neighbors.items():
            if neigh_label in self.visited:
                continue
            tentative_dist = curr_dist + neigh_dist
            if neigh_label in self.distances:
                tentative_dist = min(tentative_dist, self.distances[neigh_label])
            self.distances[neigh_label] = tentative_dist
            self.fringe.append((neigh_label, tentative_dist))
        self.visited.add(curr_label)
        self.sort_fringe()
