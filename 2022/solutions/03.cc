#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <set>

struct Item
{
    char label;
    int priority;

    Item(char label) : label(label)
    {
        int ascii_code = int(this->label);
        if (ascii_code >= 97) // lower case
        {
            this->priority = ascii_code - 96;
        }
        else // upper case
        {
            this->priority = ascii_code - 38;
        }
    }

    Item() : Item(' ') {}

    void operator=(const Item &other)
    {
        label = other.label;
        priority = other.priority;
    }

    bool operator==(const Item &other) const
    {
        return this->priority == other.priority;
    }

    bool operator<(const Item &other) const
    {
        return this->priority < other.priority;
    }
};

namespace std
{
    template <>
    struct hash<Item>
    {
        size_t operator()(const Item &item) const
        {
            return std::hash<int>()(item.priority);
        }
    };
}

class Rucksack
{
public:
    std::set<Item> items;
    Item conflict = {' '};

    Rucksack(const std::string &contents)
    {
        std::set<Item> compartment1;
        std::set<Item> compartment2;
        for (int i = 0; i < contents.length(); ++i)
        {
            Item item = {contents.at(i)};
            items.insert(item);
            if (i < contents.length() / 2)
            {
                compartment1.insert(item);
            }
            else
            {
                auto it = compartment2.insert(item).first;
                if (compartment1.contains(*it))
                {
                    this->conflict = *it;
                }
            }
        }
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::string line;
    std::vector<Rucksack> rucksacks;
    long conflict_priority_sum = 0;
    while (std::getline(input, line))
    {
        if (line == "")
            continue;
        rucksacks.emplace_back(line);
        conflict_priority_sum += rucksacks.back().conflict.priority;
    }
    std::cout << conflict_priority_sum << std::endl;
    long badge_priority_sum = 0;
    for (int i = 0; i < rucksacks.size(); i += 3)
    {
        auto &sack1 = rucksacks[i].items;
        auto &sack2 = rucksacks[i + 1].items;
        auto &sack3 = rucksacks[i + 2].items;
        std::vector<Item> first_overlaps(std::max(sack1.size(), sack2.size()));
        std::vector<Item> final_overlap(1);
        std::set_intersection(sack1.begin(), sack1.end(),
                              sack2.begin(), sack2.end(),
                              first_overlaps.begin());
        std::set_intersection(first_overlaps.begin(), first_overlaps.end(),
                              sack3.begin(), sack3.end(),
                              final_overlap.begin());
        Item &overlap = final_overlap.front();
        badge_priority_sum += overlap.priority;
    }
    std::cout << badge_priority_sum << std::endl;
}
