#include <iostream>
#include <fstream>
#include <sstream>
#include <deque>
#include <functional>
#include <typeinfo>
#include <stdexcept>
#include <tuple>
#include <set>

typedef long worry_t;

bool divides(const worry_t &left, const worry_t &right)
{
    return (left % right) == 0;
}

class Item
{
private:
    worry_t worry;

public:
    Item(const Item &) = delete;            // copy constructor
    Item &operator=(const Item &) = delete; // copy assignment
    Item(Item &&) = default;                // move constructor
    Item &operator=(Item &&) = default;     // move assignment

    Item(const worry_t &worry) : worry(worry) {}
    Item(worry_t &&worry) : worry(worry) {}

    const worry_t &Worry() const
    {
        return worry;
    }

    void Worry(worry_t &&rhs)
    {
        worry = rhs;
    }

    friend std::ostream &operator<<(std::ostream &os, const Item &rhs)
    {
        os << rhs.worry;
        return os;
    }
};

typedef std::function<worry_t(const worry_t &)> op_t;
typedef std::deque<Item> items_t;

class Monkey
{
private:
    items_t items;
    op_t op;
    worry_t test_divisor;
    std::vector<Monkey> &pack;
    size_t true_monkey;
    size_t false_monkey;
    size_t items_inspected = 0;
    long mod = 0;

public:
    Monkey(const Monkey &) = delete;            // copy constructor
    Monkey &operator=(const Monkey &) = delete; // copy assignment
    Monkey(Monkey &&) = default;                // move constructor
    Monkey &operator=(Monkey &&) = default;     // move assignment

    Monkey(items_t &&items,
           op_t &&op,
           worry_t &&test_divisor,
           std::vector<Monkey> &pack,
           size_t &&true_monkey,
           size_t &&false_monkey) : items(std::move(items)),
                                    op(std::move(op)),
                                    test_divisor(std::move(test_divisor)),
                                    pack(pack),
                                    true_monkey(std::move(true_monkey)),
                                    false_monkey(std::move(false_monkey)) {}

    const size_t &ItemsInspected() const
    {
        return items_inspected;
    }

    const long &Divisor() const
    {
        return test_divisor;
    }

    const long &Mod() const
    {
        return mod;
    }

    void Mod(const long &value)
    {
        mod = value;
    }

    Item PopFront()
    {
        Item item = std::move(items.front());
        items.pop_front();
        return item;
    }

    void PushBack(Item &&item)
    {
        // std::cout << "add: " << item << std::endl;
        items.push_back(std::move(item));
    }

    void TakeTurn()
    {
        while (!items.empty())
        {
            ++items_inspected;
            Item item = PopFront();
            item.Worry(op(item.Worry()) % mod);
            if (divides(item.Worry(), test_divisor))
            {
                pack[true_monkey].PushBack(std::move(item));
            }
            else
            {
                pack[false_monkey].PushBack(std::move(item));
            }
        }
    }

    friend std::ostream &operator<<(std::ostream &os, const Monkey &rhs)
    {
        os << "Items: ";
        for (auto &item : rhs.items)
        {
            os << item << " ";
        }
        os << std::endl;
        os << "Divisor: " << rhs.test_divisor << std::endl;
        os << "True: " << rhs.true_monkey << std::endl;
        os << "False: " << rhs.false_monkey << std::endl;
        return os;
    }
};

class MonkeyParser
{
private:
    std::istream &input;

public:
    MonkeyParser(const MonkeyParser &) = delete;            // copy constructor
    MonkeyParser &operator=(const MonkeyParser &) = delete; // copy assignment

    MonkeyParser(std::istream &input) : input(input) {}

    items_t parse_items(const std::string &line)
    {
        std::cout << "Parse items" << std::endl;
        worry_t worry;
        std::stringstream ss(line);
        items_t items;
        while (ss >> worry)
        {
            std::cout << worry << std::endl;
            items.emplace_back(worry);
            if (ss.peek() == ',')
            {
                char delim;
                ss >> delim;
            }
        }
        return items;
    }

    op_t parse_op(const std::string &line)
    {
        std::string exp = line.substr(line.find('=') + 1);
        std::string lhs;
        std::string op;
        std::string rhs;
        std::stringstream ss(exp);
        ss >> lhs >> op >> rhs;
        bool rhs_is_old = rhs == "old";
        worry_t rhs_num;
        if (!rhs_is_old)
        {
            std::stringstream convert(rhs);
            convert >> rhs_num;
        }
        if (op == "*")
        {
            if (rhs_is_old)
                return [](const worry_t &old)
                { return old * old; };
            else
                return [=](const worry_t &old)
                { return old * rhs_num; };
        }
        else if (op == "+")
        {
            if (rhs_is_old)
                return [](const worry_t &old)
                { return old + old; };
            else
                return [=](const worry_t &old)
                { return old + rhs_num; };
        }
        else
        {
            throw std::runtime_error("unknown operator");
        }
    }

    std::vector<Monkey> Parse()
    {
        std::vector<Monkey> monkeys;
        std::string line;
        std::vector<size_t> true_monkey_idxs;
        std::vector<size_t> false_monkey_idxs;
        items_t items;
        op_t op;
        worry_t divisor;
        size_t true_monkey;
        size_t false_monkey;
        while (std::getline(input, line))
        {
            std::cout << line << std::endl;
            if (line.starts_with("Monkey"))
            {
                std::cout << "start new monkey" << std::endl;
                // start of a new monkey
                continue;
            }
            if (line.size() == 0)
            {
                // finalize the monkey
                std::cout << "finalize monkey" << std::endl;
                auto &monkey = monkeys.emplace_back(std::move(items),
                                                    std::move(op),
                                                    std::move(divisor),
                                                    monkeys,
                                                    std::move(true_monkey),
                                                    std::move(false_monkey));
                std::cout << monkey << std::endl
                          << std::endl;
                continue;
            }
            size_t content_start = line.find_first_not_of(' ');
            size_t delim_loc = line.find(':');
            std::string desc = line.substr(content_start, delim_loc - content_start);
            std::string content = line.substr(delim_loc + 2);
            std::cout << "description: " << desc << std::endl;
            std::cout << "content: " << content << std::endl;
            if (desc == "Starting items")
            {
                // parse starting items
                items = parse_items(content);
                std::cout << "parsed: ";
                for (auto &item : items)
                {
                    std::cout << item << " ";
                }
                std::cout << std::endl;
            }
            if (desc == "Operation")
            {
                // parse operation
                op = parse_op(content);
            }
            if (desc == "Test")
            {
                // parse test divisor
                std::stringstream convert(content.substr(content.find_last_of(' ') + 1));
                convert >> divisor;
            }
            if (desc.starts_with("If true"))
            {
                // parse true monkey
                std::stringstream convert(content.substr(content.find_last_of(' ') + 1));
                size_t idx;
                convert >> idx;
                true_monkey = idx;
            }
            if (desc.starts_with("If false"))
            {
                // parse false monkey
                std::stringstream convert(content.substr(content.find_last_of(' ') + 1));
                size_t idx;
                convert >> idx;
                false_monkey = idx;
            }
        }
        long mod = 1;
        for (auto &monkey : monkeys)
        {
            mod *= monkey.Divisor();
        }
        for (auto &monkey : monkeys)
        {
            monkey.Mod(mod);
        }
        return monkeys;
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    MonkeyParser parser(input);
    std::vector<Monkey> monkeys = parser.Parse();
    size_t n_turns = 10000;
    for (size_t turn = 0; turn < n_turns; ++turn)
    {
        for (auto &monkey : monkeys)
        {
            monkey.TakeTurn();
        }
    }
    std::set<size_t> n_inspections;
    for (auto &monkey : monkeys)
    {
        std::cout << "Inspections: " << monkey.ItemsInspected() << std::endl;
        n_inspections.insert(monkey.ItemsInspected());
        if (n_inspections.size() > 2)
        {
            n_inspections.erase(n_inspections.begin());
        }
    }
    size_t biggest_inspection = *n_inspections.rbegin();
    size_t second_biggest_inspection = *(++n_inspections.rbegin());
    std::cout << biggest_inspection << " " << second_biggest_inspection << std::endl;
    std::cout << (biggest_inspection * second_biggest_inspection) << std::endl;
}
