from __future__ import annotations

from dataclasses import dataclass, field
from collections import deque
import re
from pprint import pprint
from operator import itemgetter
from itertools import combinations
from typing import Optional

import fire
import numpy as np
import numpy.typing as npt

import graphtools

INPUT_FORMAT = (
    r"^Valve (?P<label>[A-Z]{2}) "
    r"has flow rate=(?P<flow>\d+); "
    r"tunnels? leads? to valves? (?P<neighbors>.*)$"
)


@dataclass(frozen=True)
class Valve:
    label: str
    potential_flow_rate: int
    neighbors: dict[str, int]
    is_open: bool = False

    def flow_rate(self) -> int:
        return self.is_open * self.flow_rate


@dataclass(frozen=True)
class ValveGraph:
    valves: tuple[Valve, ...]
    label_map: dict[str, Valve]
    index_map: dict[str, int]

    def node_from_label(self, label: str) -> Valve:
        return self.label_map[label]

    @classmethod
    def from_valves(cls, valves: tuple[Valve, ...]) -> Network:
        label_map = {v.label: v for v in valves}
        index_map = {v.label: i for i, v in enumerate(valves)}
        return cls(valves=valves, label_map=label_map, index_map=index_map)


@dataclass(frozen=True)
class Network:
    graph: ValveGraph

    def compute_distances(self) -> npt.NDArray[np.int64]:
        graph = self.graph
        distances = np.ndarray(
            shape=(len(graph.valves), len(graph.valves)), dtype=np.int64
        )
        distances.fill(np.iinfo(np.int64).max)
        for valve in graph.valves:
            dijkstra = graphtools.Dijkstra(self.graph, valve)
            deque(dijkstra, 0)  # consume iterator
            print(dijkstra.distances)
            valve_idx = graph.index_map[valve.label]
            for label, dist in dijkstra.distances.items():
                other_idx = graph.index_map[label]
                distances[valve_idx, other_idx] = dist
        return distances

    def reduce(self) -> Network:
        graph = self.graph
        nonzero_valves = [
            v for v in graph.valves if v.flow_rate() > 0 or v.label == "AA"
        ]
        reduced_valves = []  # type: list[Valve]
        distances = self.compute_distances()
        for nz in nonzero_valves:
            nz_idx = graph.index_map[nz.label]
            neighs = {}  # type: dict[str, int]
            for other_nz in nonzero_valves:
                onz_idx = graph.index_map[other_nz.label]
                if nz_idx == onz_idx:
                    continue
                dist = distances[nz_idx, onz_idx]
                neighs[other_nz.label] = dist
            reduced_valves.append(Valve(nz.label, nz.flow_rate(), neighs))
        return Network(ValveGraph.from_valves(reduced_valves))


def parse_nodes(input: str) -> list[Valve]:
    valves = []  # type: list[Valve]
    with open(input) as file:
        for line in file:
            m = re.match(INPUT_FORMAT, line)
            label = m.group("label")
            flow_rate = int(m.group("flow"))
            neighs = m.group("neighbors").strip().split(", ")
            valve = Valve(
                label=label,
                flow_rate=flow_rate,
                neighbors={n: 1 for n in neighs},
            )
            valves.append(valve)
    return valves


def main(input: str):
    valves = parse_nodes(input)
    print(*valves, sep="\n")
    network = Network(ValveGraph.from_valves(valves))
    print(network.compute_distances())
    reduced = network.reduce()
    print(*reduced.graph.valves, sep="\n")
    print(reduced.compute_distances())


if __name__ == "__main__":
    fire.Fire(main)
