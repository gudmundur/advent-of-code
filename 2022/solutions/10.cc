#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>

class Comms
{
private:
    long X_register = 1l;
    std::vector<long> X_history;
    long n_cycle = 0ul;

    Comms &ClockCycle()
    {
        X_history.push_back(X_register);
        if (std::abs(X_register - CurrentPixel()) <= 1)
        {
            std::cout << "#";
        }
        else
        {
            std::cout << ".";
        }
        ++n_cycle;
        if (CurrentPixel() == 0)
        {
            std::cout << std::endl;
        }
        return *this;
    }

public:
    Comms(const Comms &) = delete;            // copy constructor
    Comms &operator=(const Comms &) = delete; // copy assignment
    Comms(Comms &&) = default;                // move constructor
    Comms &operator=(Comms &&) = default;     // move assignment

    Comms(){};

    const std::vector<long> &XHistory() const
    {
        return X_history;
    }

    const long &XRegister() const
    {
        return X_register;
    }

    const long &NCycle() const
    {
        return n_cycle;
    }

    const long CurrentPixel() const
    {
        return ((NCycle() + 1) % 40) - 1;
    }

    Comms &Noop()
    {
        return ClockCycle();
    }

    Comms &AddX(long value)
    {
        ClockCycle();
        ClockCycle();
        X_register += value;
        return *this;
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::string command;
    Comms comms;
    while (input >> command)
    {
        if (command == "noop")
        {
            comms.Noop();
        }
        if (command == "addx")
        {
            long value;
            input >> value;
            comms.AddX(value);
        }
    }
    std::cout << std::endl;
    long strength = 0;
    for (size_t idx = 19; idx < comms.XHistory().size(); idx += 40)
    {
        strength += comms.XHistory()[idx] * (idx + 1);
    }
    std::cout << strength << std::endl;
}
