#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <valarray>
#include <map>

template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &rhs)
{
    os << "[";
    for (size_t i = 0; i < rhs.size(); ++i)
    {
        os << rhs[i];
        if (i < rhs.size() - 1)
            os << ",";
    }
    os << "]";
    return os;
}

template <class T>
void rotate(std::valarray<T> &array, const size_t &col_size, const size_t &row_size)
{
    std::valarray<T> temp(array);
    for (size_t row = 0; row < row_size; ++row)
    {
        for (size_t col = 0; col < col_size; ++col)
        {
            auto idx = col + row * row_size;
            auto rotated_idx = (col_size - row - 1) + col * col_size;
            array[rotated_idx] = temp[idx];
        }
    }
}

// see if a height is visible from the left
void calculate_scenic_visibility(const std::valarray<int> &heights,
                                 const size_t &col_size,
                                 const size_t &row_size,
                                 std::valarray<bool> &visibility,
                                 std::valarray<size_t> &scores)
{

    std::valarray<int> maxes(heights);
    for (size_t row = 0; row < row_size; ++row)
    {
        auto row_idx = row * row_size;
        size_t max = heights[row_idx];
        maxes[row_idx] = max;
        scores[row_idx] = 0ul;
        visibility[row_idx] = true;
        std::map<size_t, size_t> distance_counter;
        for (size_t i = 1; i <= 10; ++i)
        {
            distance_counter[i] = 0;
        }
        for (size_t col = 1; col < col_size; ++col)
        {
            for (auto &pair : distance_counter)
            {
                ++distance_counter[pair.first];
            }
            auto idx = col + row * row_size;
            auto &left_max = maxes[idx - 1];
            auto &right = heights[idx];
            if (right > left_max)
            {
                max = right;
                visibility[idx] = true;
            }
            else
            {
                max = left_max;
            }
            maxes[idx] = max;
            scores[idx] *= (*std::min_element(distance_counter.find(right),
                                              distance_counter.end(),
                                              [](const auto &l, const auto &r)
                                              { return l.second < r.second; }))
                               .second;
            distance_counter[right] = 0;
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::vector<int> data;
    int num;
    size_t col_size = 0;
    size_t row_size = 0;
    while (input.good())
    {
        char next = input.get();
        if (next == EOF)
            break;
        if (next == '\n')
        {
            ++col_size;
            continue;
        }
        if (col_size == 0)
            ++row_size;
        std::stringstream ss;
        ss << next;
        ss >> num;
        data.push_back(num + 1);
    }
    input.close();
    // std::cout << col_size << "x" << row_size << "=" << data.size() << std::endl;
    std::valarray<int> heights(data.data(), data.size());
    std::valarray<bool> visible(false, data.size());
    std::valarray<size_t> scores(1ul, data.size());
    // left
    calculate_scenic_visibility(heights, col_size, row_size, visible, scores);
    // bottom
    rotate(heights, col_size, row_size);
    rotate(visible, col_size, row_size);
    rotate(scores, col_size, row_size);
    calculate_scenic_visibility(heights, col_size, row_size, visible, scores);
    // right
    rotate(heights, col_size, row_size);
    rotate(visible, col_size, row_size);
    rotate(scores, col_size, row_size);
    calculate_scenic_visibility(heights, col_size, row_size, visible, scores);
    // top
    rotate(heights, col_size, row_size);
    rotate(visible, col_size, row_size);
    rotate(scores, col_size, row_size);
    calculate_scenic_visibility(heights, col_size, row_size, visible, scores);
    // orient visible and scores back to normal
    rotate(visible, col_size, row_size);
    rotate(scores, col_size, row_size);
    std::cout << std::valarray<bool>(visible[visible]).size() << std::endl;
    std::cout << scores.max() << std::endl;
    return 0;
}
