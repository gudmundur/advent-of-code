#include <fstream>
#include <functional>
#include <iostream>
#include <limits>
#include <queue>
#include <set>
#include <tuple>
#include <unordered_set>
#include <vector>

typedef std::pair<size_t, size_t> index_t;

std::ostream &operator<<(std::ostream &os, const index_t &rhs)
{
    os << "(" << rhs.first << "," << rhs.second << ")";
    return os;
}

class Graph
{
private:
    std::vector<char> grid;
    size_t n_rows;
    size_t n_cols;
    size_t start;
    size_t end;
    char start_elevation = 'a';
    char end_elevation = 'z';
    std::unordered_set<size_t> seen;

public:
    Graph(const Graph &) = delete;
    Graph(Graph &&) = default;
    Graph &operator=(const Graph &) = delete;
    Graph &operator=(Graph &&) = default;

    Graph(const size_t &n_rows,
          const size_t &n_cols,
          const size_t &start,
          const size_t &end,
          std::vector<char> &&grid) : n_rows(n_rows),
                                      n_cols(n_cols),
                                      start(start),
                                      end(end),
                                      grid(std::move(grid)) {}

    size_t IndexToSize(const index_t &pair) const
    {
        auto &[row, col] = pair;
        return col + row * n_cols;
    };

    index_t SizeToIndex(size_t idx) const
    {
        return index_t(idx / n_cols, idx % n_cols);
    }

    static Graph FromInput(std::istream &input)
    {
        std::vector<char> grid;
        char buffer;
        size_t row = 0;
        size_t col = 0;
        index_t sentinel = {SIZE_MAX, SIZE_MAX};
        index_t start = sentinel;
        index_t end = sentinel;
        while (input.get(buffer))
        {
            if (buffer == '\n')
            {
                if (input.peek() != EOF)
                {
                    col = 0;
                }
                ++row;
                continue;
            }
            if (buffer == 'S')
            {
                start = {row, col};
            }
            else if (buffer == 'E')
            {
                end = {row, col};
            }
            grid.push_back(buffer);
            ++col;
        }
        if (start == sentinel || end == sentinel)
        {
            throw std::runtime_error("Failed to find start or end.");
        }
        return Graph(row,
                     col,
                     start.second + start.first * col,
                     end.second + end.first * col,
                     std::move(grid));
    }

    const std::vector<char> &Grid() const
    {
        return grid;
    }

    const std::unordered_set<size_t> &Seen() const
    {
        return seen;
    }

    void Start(const size_t &start)
    {
        this->start = start;
    }

    const char &Elevation(size_t idx) const
    {
        const char &elevation = grid.at(idx);
        if (elevation == 'S')
            return start_elevation;
        if (elevation == 'E')
            return end_elevation;
        return elevation;
    }

    index_t StartIndex() const
    {
        return SizeToIndex(start);
    }

    index_t EndIndex() const
    {
        return SizeToIndex(end);
    }

    friend std::ostream &operator<<(std::ostream &os, const Graph &graph)
    {
        for (size_t row = 0; row < graph.n_rows; ++row)
        {
            for (size_t col = 0; col < graph.n_cols; ++col)
            {
                os << graph.grid[graph.IndexToSize({row, col})];
            }
            os << std::endl;
        }
        os << graph.n_rows << "x" << graph.n_cols
           << " start:" << graph.StartIndex()
           << " end:" << graph.EndIndex();
        return os;
    }

    long Astar()
    {
        seen.clear();
        std::vector<long> distances(grid.size(), INT64_MAX);
        std::vector<long> heuristic(grid.size());
        index_t end_idx = SizeToIndex(end);
        auto &[end_row, end_col] = end_idx;
        for (size_t idx = 0; idx < grid.size(); ++idx)
        {
            index_t index = SizeToIndex(idx);
            auto &[row, col] = index;
            heuristic[idx] = std::abs((long)row - (long)end_row) +
                             std::abs((long)col - (long)end_col);
        }
        auto compare = [&](size_t l, size_t r)
        { return (distances[l]) > (distances[r]); };
        std::priority_queue<size_t,
                            std::vector<size_t>,
                            decltype(compare)>
            fringe(compare);
        fringe.push(start);
        distances[start] = 0;
        seen.insert(start);
        // std::cout << "start: " << SizeToIndex(start) << std::endl;
        while (!fringe.empty())
        {
            size_t current = std::move(fringe.top());
            fringe.pop();
            long current_distance = distances[current];
            const char &current_elevation = Elevation(current);
            // std::cout << "current: " << SizeToIndex(current) << " " << current
            //   << " " << current_elevation
            //   << " " << current_distance
            //   << std::endl;
            auto [row, col] = SizeToIndex(current);
            std::vector<size_t> neighs;
            if (row > 0)
                neighs.push_back(IndexToSize({row - 1, col}));
            if (row < n_rows - 1)
                neighs.push_back(IndexToSize({row + 1, col}));
            if (col > 0)
                neighs.push_back(IndexToSize({row, col - 1}));
            if (col < n_cols - 1)
                neighs.push_back(IndexToSize({row, col + 1}));
            for (auto &neigh : neighs)
            {
                const char &neigh_elevation = Elevation(neigh);
                // std::cout << "  check neighbor: " << SizeToIndex(neigh)
                //   << " " << neigh_elevation
                //   << std::endl;
                if ((neigh_elevation - current_elevation) <= 1)
                {
                    if (neigh == end)
                        return current_distance + 1;
                    distances[neigh] = std::min(current_distance + 1, distances[neigh]);
                    if (seen.contains(neigh))
                    {
                        // std::cout << "    seen" << std::endl;
                        continue;
                    }
                    // std::cout << "    available" << std::endl;
                    seen.insert(neigh);
                    fringe.push(neigh);
                }
            }
        }
        return -1;
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    Graph graph = Graph::FromInput(input);
    std::cout << graph.Astar() << std::endl;
    // std::cout << graph << std::endl;
    std::vector<long> trail_sizes;
    std::vector<size_t> trail_starts;
    for (size_t i = 0; i < graph.Grid().size(); ++i)
    {
        if (graph.Elevation(i) == 'a')
        {
            trail_starts.push_back(i);
        }
    }
    for (auto &start : trail_starts)
    {
        graph.Start(start);
        long len = graph.Astar();
        std::cout << graph.SizeToIndex(start) << ": " << len << std::endl;
        if (len != -1)
            trail_sizes.push_back(graph.Astar());
    }
    std::cout << *std::min_element(trail_sizes.begin(), trail_sizes.end()) << std::endl;
    // std::vector<size_t> print(graph.Seen().begin(), graph.Seen().end());
    // std::sort(print.begin(), print.end());
    // for (size_t idx = 0; idx < graph.Grid().size(); ++idx)
    // {
    //     index_t index = graph.SizeToIndex(idx);
    //     auto &[row, col] = index;
    //     if (col == 0)
    //         // std::cout << std::endl;
    //     if (graph.Seen().contains(idx))
    //         // std::cout << 'X';
    //     else
    //         // std::cout << graph.Elevation(idx);
    // }
    // std::cout << std::endl;
    return 0;
}
