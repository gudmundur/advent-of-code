#include <fstream>
#include <iostream>
#include <tuple>
#include <vector>
#include <string>

typedef std::pair<long, long> interval;

bool contains(interval left, interval right)
{
    return left.first <= right.first && left.second >= right.second;
}

bool contains(std::pair<interval, interval> intervals)
{
    return contains(intervals.first, intervals.second) || contains(intervals.second, intervals.first);
}

bool overlaps(interval left, interval right)
{
    return (left.first <= right.first && left.second >= right.first) || (right.first <= left.first && right.second >= left.first);
}

interval parse_interval(const std::string &str)
{
    std::string delimiter = "-";
    size_t del_pos = str.find(delimiter);
    interval parsed = {std::stol(str.substr(0, del_pos)),
                       std::stol(str.substr(del_pos + 1))};
    return parsed;
}

std::pair<interval, interval> parse_line(const std::string &line)
{
    std::string delimiter = ",";
    size_t del_pos = line.find(delimiter);
    std::pair<interval, interval> parsed = {parse_interval(line.substr(0, del_pos)),
                                            parse_interval(line.substr(del_pos + 1))};
    return parsed;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::string line;
    std::vector<std::pair<interval, interval>> assignment_pairs;
    long n_contains = 0;
    long n_overlaps = 0;
    if (input.is_open())
    {
        while (input.good())
        {
            std::getline(input, line);
            if (line == "")
                continue;
            assignment_pairs.push_back(parse_line(line));
            if (contains(assignment_pairs.back()))
            {
                ++n_contains;
            }
            if (overlaps(assignment_pairs.back().first, assignment_pairs.back().second))
            {
                ++n_overlaps;
            }
        }
    }
    std::cout << n_contains << std::endl;
    std::cout << n_overlaps << std::endl;
    return 0;
}
