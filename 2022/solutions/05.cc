#include <vector>
#include <deque>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

typedef std::vector<std::deque<char>> stacks_t;

void populate_stacks(stacks_t &stacks, const std::string &line)
{
    for (size_t i = 0; i < stacks.size(); ++i)
    {
        auto &stack = stacks[i];
        auto &val = line.at(i * 4 + 1);
        if (val == ' ')
            continue;
        stack.push_back(line.at(i * 4 + 1));
    }
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::stringstream ss;
    std::string line;
    stacks_t stacks;
    std::getline(input, line);
    // each item has 4 chars except last one
    size_t num_stacks = line.size() / 4 + 1;
    for (size_t i = 0; i < num_stacks; ++i)
    {
        stacks.push_back(std::deque<char>());
    }
    populate_stacks(stacks, line);
    while (input.is_open() && input.good())
    {
        std::getline(input, line);
        if (line == "" || line.at(1) == '1')
            break;
        populate_stacks(stacks, line);
    }
    for (auto &stack : stacks)
    {
        std::cout << "stack:";
        for (auto &item : stack)
        {
            std::cout << " " << item;
        }
        std::cout << std::endl;
    }
    std::getline(input, line); // numbers
    std::string dummy;
    size_t n_move;
    size_t origin_idx;
    size_t dest_idx;
    while (input >> dummy >> n_move >> dummy >> origin_idx >> dummy >> dest_idx)
    {
        std::cout << n_move << ": " << origin_idx << "->" << dest_idx << std::endl;
        auto &origin = stacks[origin_idx - 1];
        auto &dest = stacks[dest_idx - 1];
        std::vector<char> crane(origin.rbegin() + (origin.size() - n_move), origin.rend());
        origin.erase(origin.begin(), origin.begin() + n_move);
        for (auto &item : crane)
        {
            dest.push_front(item);
        }
    }
    for (auto &stack : stacks)
    {
        std::cout << "stack:";
        for (auto &item : stack)
        {
            std::cout << " " << item;
        }
        std::cout << std::endl;
    }
    return 0;
}