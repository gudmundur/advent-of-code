#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "point.hh"
#include "grid.hh"
#include "spline.hh"

std::istream &operator>>(std::istream &is, Point &rhs)
{
    long x, y;
    is >> x;
    is.ignore(1, ',');
    is >> y;
    rhs = {x, y};
    return is;
}

std::istream &operator>>(std::istream &is, Spline &rhs)
{
    rhs.Clear();
    Point point;
    std::string delim;
    is >> point;
    point = Point(point.Y(), point.X());
    rhs.PushBack(std::move(point));
    while (is >> delim >> point)
    {
        point = Point(point.Y(), point.X());
        rhs.PushBack(std::move(point));
    }
    return is;
}

class SandSimulator
{
private:
    Point origin;
    Grid grid;
    Point sand = {-1, -1};
    bool out_of_bounds = false;

    void AddWall(const Point &left, const Point &right)
    {
        Point direction = (right - left).Direction();
        for (Point current = left; current != right; current += direction)
        {
            grid(current.Coordinate()) = '#';
        }
        grid(right.Coordinate()) = '#';
    }

    void AddWall(const Spline &wall)
    {
        if (wall.Vertices().empty())
        {
            return;
        }
        const Point &left = wall.Vertices()[0];
        if (wall.Vertices().size() == 1)
        {
            grid(left.Coordinate()) = '#';
            return;
        }
        for (size_t idx = 1; idx < wall.Vertices().size(); ++idx)
        {
            AddWall(wall.Vertices()[idx - 1], wall.Vertices()[idx]);
        }
    }

    bool MoveSand(const Point &current, const Point &next)
    {
        auto &[next_row, next_col] = next.Coordinate();
        // std::cout << "next: " << next.Coordinate() << std::endl;
        out_of_bounds = next_row > grid.NRows() - 1 ||
                        next_row < 0 ||
                        next_col > grid.NCols() - 1 ||
                        next_col < 0;
        bool can_move = grid(next) == '.';
        if (out_of_bounds || can_move)
        {
            if (current == origin)
                grid(current) = '+';
            else
                grid(current) = '.';
        }
        if (can_move)
        {
            grid(next) = 'o';
            return true;
        }
        return false;
    }

public:
    SandSimulator(const SandSimulator &) = delete;
    SandSimulator(SandSimulator &&) = default;

    SandSimulator &operator=(const SandSimulator &) = delete;
    SandSimulator &operator=(SandSimulator &&) = default;

    SandSimulator(const Point &origin, const std::vector<Spline> &walls)
    {
        Spline bounding_box;
        bounding_box.PushBack(Point(origin));
        for (auto &wall : walls)
        {
            bounding_box.Combine(wall);
        }
        bounding_box = bounding_box.BoundingBox();
        std::cout << "bounding box: " << bounding_box << std::endl;
        Point bottom_left = bounding_box.Vertices()[0];
        Point top_right = bounding_box.Vertices()[2];
        // create the floor
        long height = top_right.X();
        Spline floor;
        floor.PushBack({height + 2, origin.Y() - height - 2});
        floor.PushBack({height + 2, origin.Y() + height + 2});
        bounding_box.Combine(floor);
        bounding_box = bounding_box.BoundingBox();
        bottom_left = bounding_box.Vertices()[0];
        top_right = bounding_box.Vertices()[2];
        std::vector<Spline> shifted_walls;
        shifted_walls.push_back(floor.Shift(bottom_left));
        for (auto &wall : walls)
        {
            shifted_walls.push_back(wall.Shift(bottom_left));
            std::cout << wall.Shift(bottom_left) << std::endl;
        }
        this->origin = origin;
        this->origin -= bottom_left;
        std::cout << "origin: " << this->origin << std::endl;
        auto &[row_max, col_max] = (top_right - bottom_left).Coordinate();
        size_t n_cols = col_max + 1;
        size_t n_rows = row_max + 1;
        std::vector<char> data(n_cols * n_rows, '.');
        this->grid = Grid(std::move(data), n_cols, n_rows);
        this->grid(this->origin.Coordinate()) = '+';
        for (auto &wall : shifted_walls)
        {
            AddWall(wall);
        }
    }

    const bool &OutOfBounds() const
    {
        return out_of_bounds;
    }

    bool Iterate()
    {
        if (out_of_bounds)
        {
            throw std::runtime_error("already out of bounds");
        }
        bool moved = false;
        if (sand == Point(-1, -1))
        {
            sand = origin;
        }
        Point current = sand;
        // std::cout << "current: " << current << std::endl;
        Point next = sand + Point(1, 0);
        moved = MoveSand(current, next);
        if (!moved)
        {
            next = current + Point(1, -1);
            moved = MoveSand(current, next);
            if (!moved)
            {
                next = current + Point(1, 1);
                moved = MoveSand(current, next);
            }
        }
        if (moved)
            sand = next;
        else
        {
            if (current == origin)
            {
                out_of_bounds = true;
                this->grid(origin) = 'o';
            }
            sand = Point(-1, -1);
        }
        // std::cout << "moved: " << moved << std::endl;
        return moved;
    }

    const Grid &GetGrid() const
    {
        return grid;
    }

    size_t CountSand() const
    {
        size_t sum = 0;
        for (auto &c : grid.Data())
        {
            if (c == 'o')
                ++sum;
        }
        return sum;
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::string line;
    std::vector<Spline> splines;
    Spline spline;
    while (std::getline(input, line))
    {
        if (line == "")
            continue;
        std::cout << line << std::endl;
        std::stringstream ls_input(line);
        ls_input >> spline;
        splines.push_back(std::move(spline));
    }
    std::cout << "Splines:" << std::endl;
    for (auto &seg : splines)
    {
        std::cout << "  " << seg << std::endl;
    }
    Point origin = {0, 500};
    SandSimulator sim(origin, splines);
    std::cout << sim.GetGrid() << std::endl;
    while (!sim.OutOfBounds())
    {
        while (sim.Iterate())
        {
        }
        // std::cout << std::endl;
    }
    std::cout << sim.GetGrid() << std::endl;
    std::cout << sim.CountSand() << std::endl;
    return 0;
}
