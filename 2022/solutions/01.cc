#include <fstream>
#include <iostream>
#include <set>
#include <sstream>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::string line;
    std::set<long> top_3;
    long calories = 0;
    long buffer;
    while (std::getline(input, line))
    {
        if (line == "")
        {
            top_3.insert(calories);
            if (top_3.size() > 3)
            {
                top_3.erase(top_3.begin());
            }
            calories = 0;
            continue;
        }
        std::stringstream convert(line);
        convert >> buffer;
        calories += buffer;
    }
    long sum = 0;
    for (auto &c : top_3)
    {
        sum += c;
    }
    auto &top = *top_3.rbegin();
    std::cout << top << std::endl;
    std::cout << sum << std::endl;
    return 0;
}
