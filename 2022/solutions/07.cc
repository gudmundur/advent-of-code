#include <deque>
#include <fstream>
#include <sstream>

#include "filesystem.hh"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    FileSystem fs;
    std::string line;
    while (std::getline(input, line))
    {
        if (line.at(0) == '$')
        {
            std::stringstream buffer;
            std::string dummy;
            std::string cmd;
            std::string dir;
            buffer << line;
            buffer >> dummy >> cmd;
            if (cmd == "cd")
            {
                buffer >> dir;
                fs.ChangeDir(dir);
            }
            else if (cmd == "ls")
            {
                while (input.peek() != '$' && std::getline(input, line))
                {
                    std::stringstream file_name_buffer;
                    file_name_buffer << line;
                    std::string type_or_size;
                    std::string name;
                    file_name_buffer >> type_or_size >> name;
                    if (type_or_size == "dir")
                    {
                        fs.Cwd().CreateSubdirectory(name);
                    }
                    else
                    {
                        std::stringstream ts;
                        ts << type_or_size;
                        size_t size;
                        ts >> size;
                        fs.Cwd().CreateFile(name, size);
                    }
                }
            }
        }
    }
    std::cout << fs << std::endl;
    size_t max_fs_size = 70000000ul;
    size_t fs_size = fs.Root().TotalSize();
    size_t space_needed = 30000000ul;
    size_t min_space = space_needed - (max_fs_size - fs_size);
    size_t best_size = fs_size;
    std::deque<const FileSystemObject *> stack;
    stack.push_back(&fs.Root());
    while (!stack.empty())
    {
        auto &obj = *stack.front();
        size_t size = obj.TotalSize();
        if (size > min_space && size < best_size)
        {
            best_size = size;
        }
        for (auto &child : obj.Contents())
        {
            if (child.second.IsDirectory())
            {
                stack.push_back(&child.second);
            }
        }
        stack.pop_front();
    }
    std::cout << fs_size << std::endl;
    std::cout << min_space << std::endl;
    std::cout << best_size << std::endl;
    return 0;
}
