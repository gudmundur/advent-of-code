#include <iostream>
#include <fstream>
#include <vector>
#include <limits>
#include <unordered_set>
#include <sstream>

#include "point.hh"
#include "spline.hh"

constexpr auto STREAM_MAX = std::numeric_limits<std::streamsize>::max();

std::istream &operator>>(std::istream &is, Point &rhs)
{
    long x, y;
    is.ignore(STREAM_MAX, '=');
    is >> y;
    is.ignore(STREAM_MAX, '=');
    is >> x;
    rhs = {x, y};
    return is;
}

template <class T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &rhs)
{
    for (auto &item : rhs)
    {
        os << item << " ";
    }
    return os;
}

bool operator<(const Point &left, const Point &right)
{
    if (left.X() < right.X())
        return true;
    if (left.X() == right.X())
        return left.Y() < right.Y();
    return false;
}

class SensorField
{
private:
    std::vector<Point> sensors;
    std::vector<Point> beacons;
    std::unordered_set<Point> unique_beacons;
    std::vector<long> distances;
    Spline bounding_box;
    long bound;

public:
    SensorField(const SensorField &) = delete;
    SensorField(SensorField &&) = default;

    SensorField &operator=(const SensorField &) = delete;
    SensorField &operator=(SensorField &&) = default;

    SensorField(const std::vector<Point> &sensors,
                const std::vector<Point> &beacons,
                long bound) : sensors(sensors),
                              beacons(beacons),
                              unique_beacons(beacons.begin(), beacons.end()),
                              bound(bound)
    {
        if (sensors.size() != beacons.size())
        {
            throw std::runtime_error("must have sensor size same as beacon sise");
        }
        for (size_t idx = 0; idx < sensors.size(); ++idx)
        {
            auto &sensor = sensors.at(idx);
            auto &beacon = beacons.at(idx);
            long distance = norm<1>(sensor - beacon);
            distances.push_back(distance);
            bounding_box.PushBack(sensor);
            bounding_box.PushBack(beacon);
            bounding_box.PushBack(sensor - Point(distance, 0));
            bounding_box.PushBack(sensor + Point(distance, 0));
            bounding_box.PushBack(sensor - Point(0, distance));
            bounding_box.PushBack(sensor + Point(0, distance));
        }
        bounding_box = bounding_box.BoundingBox();
        std::cout << "bounds: " << bounding_box << std::endl;
        std::cout << "distances: " << distances << std::endl;
    }

    const Spline &BoundingBox() const
    {
        return bounding_box;
    }

    const std::vector<Point> &Sensors() const
    {
        return sensors;
    }

    const std::vector<Point> &Beacons() const
    {
        return beacons;
    }

    bool CanContainBeacon(const long &row, const long &col) const
    {
        Point point(row, col);
        // std::cout << "check: " << point << std::endl;
        auto &bottom_left = bounding_box.Vertices()[0];
        auto &top_right = bounding_box.Vertices()[2];
        if (row < bottom_left.X() || row > top_right.X() ||
            col < bottom_left.Y() || col > top_right.Y())
        {
            return false;
        }
        for (size_t idx = 0; idx < sensors.size(); ++idx)
        {
            auto &sensor = sensors.at(idx);
            auto &beacon_distance = distances.at(idx);
            if (norm<1>(sensor - point) <= beacon_distance)
            {
                return false;
            }
        }
        return true;
    }

    bool CanContainBeacon(const Point &point) const
    {
        return CanContainBeacon(point.X(), point.Y());
    }

    size_t CountNonBeaconPositions(const long &row) const
    {
        size_t sum = 0;
        auto &bottom_left = bounding_box.Vertices()[0];
        auto &top_right = bounding_box.Vertices()[2];
        for (long col = bottom_left.Y(); col <= top_right.Y(); ++col)
        {
            if (!CanContainBeacon(row, col))
            {
                ++sum;
            }
        }
        for (auto &beacon : unique_beacons)
        {
            if (beacon.X() == row)
            {
                --sum;
            }
        }
        return sum;
    }

    Point FindPossibleBeaconPosition() const
    {
        std::vector<Point> to_check;
        for (size_t idx = 0; idx < sensors.size(); ++idx)
        {
            auto &sensor = sensors[idx];
            auto &distance = distances[idx];
            Point top = sensor + Point(distance, 0);
            Point bottom = sensor - Point(distance, 0);
            Point left = sensor - Point(0, distance);
            Point right = sensor + Point(0, distance);
            std::vector<Point> possible;
            for (long row_offset = 1; row_offset <= distance; ++row_offset)
            {
                possible.push_back(sensor + Point(row_offset, distance + 1 - row_offset));
                possible.push_back(sensor + Point(row_offset, -(distance + 1 - row_offset)));
                possible.push_back(sensor + Point(-row_offset, distance + 1 - row_offset));
                possible.push_back(sensor + Point(-row_offset, -(distance + 1 - row_offset)));
            }
            possible.push_back(sensor + Point(0, -distance - 1));
            possible.push_back(sensor + Point(0, distance + 1));
            for (auto &p : possible)
            {
                auto &[row, col] = p.Coordinate();
                if (row >= 0 && row <= bound && col >= 0 && col <= bound)
                {
                    to_check.push_back(p);
                }
            }
        }
        std::cout << "check: " << to_check.size() << std::endl;
        for (auto &c : to_check)
        {
            if (CanContainBeacon(c))
            {
                return c;
            }
        }
        return Point(-1, -1);
    }
};

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::vector<Point> sensors, beacons;
    std::string line;
    while (std::getline(input, line))
    {
        if (line == "")
            continue;
        std::stringstream buffer(line);
        Point sensor, beacon;
        buffer >> sensor >> beacon;
        sensors.push_back(sensor);
        beacons.push_back(beacon);
    }
    std::cout << "sensors: " << sensors << std::endl;
    std::cout << "beacons: " << beacons << std::endl;
    SensorField field(sensors, beacons, 4000000);
    std::cout << field.CountNonBeaconPositions(2000000) << std::endl;
    Point pos = field.FindPossibleBeaconPosition();
    std::cout << pos << std::endl;
    std::cout << (pos.X() + 4000000 * pos.Y()) << std::endl;
    return 0;
}
