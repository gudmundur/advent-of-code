#include <cmath>
#include <iostream>
#include <fstream>
#include <tuple>
#include <unordered_set>
#include <unordered_map>
#include <vector>

#include "point.hh"
#include "rope.hh"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    std::unordered_map<char, Direction> dir_map{
        {'R', RIGHT},
        {'L', LEFT},
        {'D', DOWN},
        {'U', UP}};
    char dir_code;
    size_t num;
    Rope rope(10);
    while (input >> dir_code >> num)
    {
        for (size_t i = 0; i < num; ++i)
        {
            rope.MoveHead(dir_map.at(dir_code));
        }
    }
    std::cout << rope.TailTrail().size() << std::endl;
    return 0;
}
