import copy
import functools
import itertools
import logging
from pprint import pprint
from typing import List

import fire

_log = logging.getLogger(__name__)


def compare(left, right) -> int:
    if isinstance(left, int) and isinstance(right, int):
        return right - left
    if isinstance(left, list) and isinstance(right, list):
        if not left and right:
            return 1
        if left and not right:
            return -1
        if not left and not right:
            return 0
        l, r = left.pop(0), right.pop(0)
        result = compare(l, r)
        if result != 0:
            return result
        return compare(left, right)
    if not isinstance(left, list):
        left = [left]
    if not isinstance(right, list):
        right = [right]
    return compare(left, right)


def is_valid(left: List, right: List):
    if not left and right:
        return True
    if left and not right:
        return False
    l, r = left.pop(0), right.pop(0)
    result = compare(l, r)
    if result != 0:
        return result > 0
    return is_valid(left, right)


def main(filename: str):
    _log.info(f"file: {filename}")
    pairs = []
    with open(filename) as file:
        while file.readable():
            packet1 = next(file).strip()
            packet2 = next(file).strip()
            pairs.append((eval(packet1), eval(packet2)))
            _log.info("packet1: %s", packet1)
            _log.info("packet2: %s", packet2)
            try:
                if next(file).strip() != "":
                    raise ValueError("something went wrong")
            except StopIteration:
                break
    pprint(pairs)
    mysum = 0
    for i, packet in enumerate(pairs):
        mysum += is_valid(*copy.deepcopy(packet)) * (i + 1)
    print(mysum)
    packets = list(itertools.chain.from_iterable(pairs))
    pprint(packets)
    packets.append([[2]])
    packets.append([[6]])
    packets.sort(
        key=functools.cmp_to_key(
            lambda l, r: -compare(copy.deepcopy(l), copy.deepcopy(r))
        )
    )
    print()
    pprint(packets)
    print((packets.index([[2]]) + 1) * (packets.index([[6]]) + 1))


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    fire.Fire(main)
