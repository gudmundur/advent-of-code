#include <array>
#include <iostream>
#include <fstream>
#include <unordered_map>

enum Move
{
    ROCK,
    PAPER,
    SCISSORS
};

enum Outcome
{
    LOSE,
    DRAW,
    WIN
};

const std::array<std::array<Outcome, 3>, 3> OutcomeMatrix = {
    {{DRAW, LOSE, WIN},
     {WIN, DRAW, LOSE},
     {LOSE, WIN, DRAW}}};

const std::array<std::array<Move, 3>, 3> MoveMatrix = {
    {{SCISSORS, ROCK, PAPER},
     {ROCK, PAPER, SCISSORS},
     {PAPER, SCISSORS, ROCK}}};

long get_score(const Move &move, const Outcome &outcome)
{
    long move_score = move + 1;
    long outcome_score = 3 * outcome;
    return move_score + outcome_score;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    const std::unordered_map<char, Move> move_map = {{'A', ROCK},
                                                     {'B', PAPER},
                                                     {'C', SCISSORS}};
    const std::unordered_map<char, Outcome> outcome_map = {{'X', LOSE},
                                                           {'Y', DRAW},
                                                           {'Z', WIN}};
    char move_code;
    char outcome_code;
    long score = 0;
    while (input >> move_code >> outcome_code)
    {
        Move opponents_move = move_map.at(move_code);
        Outcome outcome = outcome_map.at(outcome_code);
        Move move = MoveMatrix.at(opponents_move).at(outcome);
        score += get_score(move, outcome);
    }
    std::cout << score << std::endl;
}
