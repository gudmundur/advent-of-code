#include <fstream>
#include <iostream>

#include "message_marker.hh"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        std::cout << "usage: " << argv[0] << " <input file>" << std::endl;
        return 1;
    }
    std::ifstream input(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "failed to open input " << argv[1] << std::endl;
        return -1;
    }
    MessageMarker marker(14);
    char buffer;
    while ((input >> buffer))
    {
        marker.Push(buffer);
        if (marker.MarkerFound())
        {
            std::cout << marker.GetNProcessed() << std::endl;
            return 0;
        }
    }
    return 0;
}
