from __future__ import annotations

from dataclasses import dataclass, field, replace
from enum import IntEnum

import fire
import numpy as np
from numpy.typing import NDArray
from tqdm import tqdm


class NodeType(IntEnum):
  EMPTY = 0
  OBSTACLE = 1


class Orientation(IntEnum):
  UP = 0
  RIGHT = 1
  DOWN = 2
  LEFT = 3


@dataclass(frozen=True, eq=True)
class Guard:
  coordinate: tuple[int, int]
  orientation: Orientation

  def rotate(self) -> Guard:
    match self.orientation:
      case Orientation.UP:
        new = Orientation.RIGHT
      case Orientation.RIGHT:
        new = Orientation.DOWN
      case Orientation.DOWN:
        new = Orientation.LEFT
      case Orientation.LEFT:
        new = Orientation.UP
    return replace(self, orientation=new)

  def facing_coord(self) -> tuple[int, int]:
    y, x = self.coordinate
    match self.orientation:
      case Orientation.UP:
        return y - 1, x
      case Orientation.RIGHT:
        return y, x + 1
      case Orientation.DOWN:
        return y + 1, x
      case Orientation.LEFT:
        return y, x - 1

  def advance(self) -> Guard:
    return replace(self, coordinate=self.facing_coord())


@dataclass(frozen=True)
class Node:
  type: NodeType


@dataclass
class Grid:
  nodes: list[list[Node]]
  guard: Guard
  data: NDArray[np.str_]
  loop: bool = False
  visited: set[Guard] = field(init=False)

  def __post_init__(self):
    self.visited = set()

  def iterate(self) -> bool:
    if self.loop:
      return False
    current_y, current_x = self.guard.coordinate
    next_y, next_x = self.guard.facing_coord()
    dim_y, dim_x = self.data.shape
    if next_x < 0 or next_y < 0 or next_x >= dim_x or next_y >= dim_y:
      self.data[current_y, current_x] = "X"
      return False
    match self.nodes[next_y][next_x]:
      case Node(type=NodeType.OBSTACLE):
        self.guard = self.guard.rotate()
      case Node(type=NodeType.EMPTY):
        self.guard = self.guard.advance()
        self.data[current_y, current_x] = "X"
        self.data[next_y, next_x] = "^"
    if self.guard in self.visited:
      self.loop = True
      return False
    self.visited.add(self.guard)
    return True

  @classmethod
  def parse(cls, s: str) -> Grid:
    lines = s.splitlines()
    data: NDArray[np.str_] = np.array(lines, dtype="U").view("U1")
    n_rows, n_cols = len(lines), len(lines[0])
    data.resize((n_rows, n_cols))
    return cls.from_data(data)

  @classmethod
  def from_data(cls, data: NDArray[np.str_]) -> Grid:
    nodes: list[list[Node]] = []
    guard = None
    for row_idx, char_row in enumerate(data):
      row: list[Node] = []
      for col_idx, char in enumerate(char_row):
        node = None
        match char:
          case ".":
            node = Node(NodeType.EMPTY)
          case "#":
            node = Node(NodeType.OBSTACLE)
          case "^":
            guard = Guard(coordinate=(row_idx, col_idx), orientation=Orientation.UP)
            node = Node(NodeType.EMPTY)
        if node is not None:
          row.append(node)
      nodes.append(row)
    if guard is None:
      raise RuntimeError("failed to find a guard")
    return Grid(nodes=nodes, guard=guard, data=data)

  def __str__(self) -> str:
    return "\n".join("".join(row) for row in self.data)


def part2(filename: str) -> int:
  with open(filename, "r") as infile:
    grid = Grid.parse(infile.read())
  guard_coord = grid.guard.coordinate
  data = grid.data.copy()
  while grid.iterate():
    pass
  y_idxs, x_idxs = np.indices(data.shape, dtype=np.int64)
  circuit_mask = grid.data == "X"
  y_idxs = y_idxs[circuit_mask]
  x_idxs = x_idxs[circuit_mask]
  count_ = 0
  for y_idx, x_idx in tqdm(list(zip(y_idxs, x_idxs))):
    if (y_idx, x_idx) == guard_coord:
      continue
    variant_data = data.copy()
    variant_data[y_idx, x_idx] = "#"
    grid = Grid.from_data(variant_data)
    while grid.iterate():
      pass
    if grid.loop:
      count_ += 1
  return count_


def part1(filename: str) -> int:
  with open(filename, "r") as infile:
    grid = Grid.parse(infile.read())
  while grid.iterate():
    pass
  return (grid.data == "X").sum()


def main(filename: str):
  print(part1(filename))
  print(part2(filename))


if __name__ == "__main__":
  fire.Fire(main)
