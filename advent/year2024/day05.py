# allow self referential annotation
from __future__ import annotations

from dataclasses import dataclass, field
from functools import cache
from graphlib import TopologicalSorter
from itertools import chain
from typing import Iterator

import fire


@dataclass
class Node:
  value: int
  parents: set[Node] = field(default_factory=set)

  @cache
  def transitive_parents(self, relevant: frozenset[Node]) -> set[Node]:
    added: set[Node] = set()
    to_add = set(self.parents & relevant)
    while to_add:
      parent = to_add.pop()
      added.add(parent)
      to_add |= (parent.parents - added) & relevant
    return added

  def __eq__(self, other):
    return self.value == other.value

  def __hash__(self):
    return hash(self.value)


@dataclass
class Graph:
  nodes: dict[int, Node] = field(default_factory=dict)

  def add(self, node: Node) -> Graph:
    self.nodes[node.value] = node
    return self


def is_valid_update(update: tuple[Node, ...], graph: Graph) -> bool:
  preceding: set[Node] = set()
  relevant = frozenset(update)
  for node in update:
    if not node.transitive_parents(relevant) <= preceding:
      return False
    preceding.add(node)
  return True


def part1(graph: Graph, updates: list[tuple[Node, ...]]) -> int:
  return sum(update[len(update) // 2].value for update in updates if is_valid_update(update, graph))


def correct_update(update: tuple[Node, ...]) -> tuple[int, ...]:
  sorter = TopologicalSorter()
  relevant = frozenset(update)
  for node in update:
    sorter.add(node.value, *[parent.value for parent in node.transitive_parents(relevant)])
  return tuple(sorter.static_order())


def part2(graph: Graph, updates: list[tuple[Node, ...]]) -> int:
  sum_ = 0
  for update in updates:
    if is_valid_update(update, graph):
      continue
    corrected = correct_update(update)
    sum_ += corrected[len(corrected) // 2]
  return sum_


def main(filename: str):
  dependencies: list[tuple[int, int]] = []
  updates: list[tuple[int, ...]] = []
  with open(filename, "r") as infile:
    lines: Iterator[str] = map(lambda line: line.strip(), iter(infile))
    for line in lines:
      if not line:
        break
      parent, child = line.split("|")
      dependencies.append((int(parent), int(child)))
    for line in lines:
      updates.append(tuple(int(c) for c in line.split(",")))
  graph = Graph()
  for node_val in set(chain(chain.from_iterable(dependencies), chain.from_iterable(updates))):
    graph.add(Node(value=node_val))
  for parent_val, child_val in dependencies:
    parent = graph.nodes[parent_val]
    child = graph.nodes[child_val]
    child.parents.add(parent)
  node_updates = [tuple(graph.nodes[u] for u in update) for update in updates]
  print(part1(graph, node_updates))
  print(part2(graph, node_updates))


if __name__ == "__main__":
  fire.Fire(main)
