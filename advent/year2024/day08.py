import math
from dataclasses import dataclass
from itertools import combinations
from typing import cast

import fire
import numpy as np


@dataclass(frozen=True)
class Antenna:
  symbol: str
  coord: tuple[int, int]


def get_antinodes(a1: Antenna, a2: Antenna, bounds: tuple[int, int]) -> list[tuple[int, int]]:
  zeros = np.zeros((2,), dtype=np.int64)
  limit = np.array(bounds, dtype=np.int64).reshape((2,))
  c1 = np.array(a1.coord, dtype=np.int64).reshape((2,))
  c2 = np.array(a2.coord, dtype=np.int64).reshape((2,))
  diff = c1 - c2
  diff //= math.gcd(*cast(list[int], diff.tolist()))
  antinodes: set[tuple[int, int]] = set()
  antinode = c1.copy()
  while np.all(antinode >= zeros) and np.all(antinode < limit):
    antinodes.add(cast(tuple[int, int], tuple(cast(list[int], antinode.tolist()))))
    antinode += diff
  antinode = c1.copy()
  while np.all(antinode >= zeros) and np.all(antinode < limit):
    antinodes.add(cast(tuple[int, int], tuple(cast(list[int], antinode.tolist()))))
    antinode -= diff
  return list(antinodes)


def get_antinodes_simple(a1: Antenna, a2: Antenna) -> list[tuple[int, int]]:
  c1 = np.array(a1.coord, dtype=np.int64)
  c2 = np.array(a2.coord, dtype=np.int64)
  an1 = cast(tuple[int, int], tuple(cast(list[int], (2 * c1 - c2).tolist())))
  an2 = cast(tuple[int, int], tuple(cast(list[int], (2 * c2 - c1).tolist())))
  return [an1, an2]


def part2(antennas: dict[str, set[Antenna]], bounds: tuple[int, int]) -> int:
  antinodes: set[tuple[int, int]] = set()
  for antenna_set in antennas.values():
    for a1, a2 in combinations(antenna_set, 2):
      antinodes |= set(get_antinodes(a1, a2, bounds))
  return len(antinodes)


def part1(antennas: dict[str, set[Antenna]], bounds: tuple[int, int]) -> int:
  n_rows, n_cols = bounds
  antinodes: set[tuple[int, int]] = set()
  for antenna_set in antennas.values():
    for a1, a2 in combinations(antenna_set, 2):
      antinodes |= set(get_antinodes_simple(a1, a2))
  sum_ = 0
  for ay, ax in antinodes:
    if ay >= 0 and ax >= 0 and ay < n_rows and ax < n_cols:
      sum_ += 1
  return sum_


def main(filename: str):
  with open(filename, "r") as infile:
    data = infile.read()
  antennas: dict[str, set[Antenna]] = {}
  lines = data.splitlines()
  n_rows, n_cols = len(lines), len(lines[0])
  grid = np.array(lines, dtype="U").view("U1").reshape((n_rows, n_cols))
  for row_idx, line in enumerate(lines):
    for col_idx, char in enumerate(line):
      if char != ".":
        if not char in antennas:
          antennas[char] = set()
        antennas[char].add(Antenna(symbol=char, coord=(row_idx, col_idx)))
  print(part1(antennas, (n_rows, n_cols)))
  print(part2(antennas, (n_rows, n_cols)))


if __name__ == "__main__":
  fire.Fire(main)
