from dataclasses import dataclass
from functools import cache

import fire


@dataclass(frozen=True)
class Equation:
  test: int
  numbers: tuple[int, ...]

  @cache
  def is_feasible(self, concat: bool = False) -> bool:
    if len(self.numbers) == 1:
      return self.test == self.numbers[0]
    if self.numbers[0] > self.test:
      return False
    children: list[Equation] = []
    first, second = self.numbers[:2]
    mult = first * second
    add = first + second
    rest = self.numbers[2:]
    children.extend([Equation(self.test, (mult, *rest)), Equation(self.test, (add, *rest))])
    if concat:
      concatted = int(f"{first}{second}")
      children.append(Equation(self.test, (concatted, *rest)))
    return any(c.is_feasible(concat) for c in children)


def part1(equations: list[Equation]) -> int:
  sum_ = 0
  for eq in equations:
    sum_ += eq.test * eq.is_feasible()
  return sum_


def part2(equations: list[Equation]) -> int:
  sum_ = 0
  for eq in equations:
    sum_ += eq.test * eq.is_feasible(concat=True)
  return sum_


def main(filename: str):
  equations: list[Equation] = []
  with open(filename, "r") as infile:
    for line in infile:
      nums_raw = line.strip().split(" ")
      test = int(nums_raw[0][:-1])
      numbers = tuple(int(n) for n in nums_raw[1:])
      equations.append(Equation(test=test, numbers=numbers))
  print(part1(equations))
  print(part2(equations))


if __name__ == "__main__":
  fire.Fire(main)
