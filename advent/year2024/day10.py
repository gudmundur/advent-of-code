from __future__ import annotations

from dataclasses import dataclass
from itertools import chain

import fire
import numpy as np
from numpy.typing import NDArray


@dataclass(frozen=True)
class Trailhead:
  start: NDArray[np.int64]
  score: int


class Grid:
  def __init__(self, data: NDArray[np.int64]):
    self.data = data

  def __str__(self) -> str:
    return "\n".join(" ".join(str(i) for i in line) for line in self.data.tolist())

  @classmethod
  def from_str(cls, s: str) -> Grid:
    lines = s.splitlines()
    data = np.array([int(c) for c in chain.from_iterable(lines)], dtype=np.int64).reshape(
      (len(lines), len(lines[0]))
    )
    return cls(data=data)


def part1(grid: Grid) -> int:
  print(grid)
  return 0


def main(filename: str):
  with open(filename, "r") as infile:
    grid = Grid.from_str(infile.read())
  print(part1(grid))


if __name__ == "__main__":
  fire.Fire(main)
