import re
from itertools import chain
from typing import Any, Iterator

import fire
import numpy as np
from numpy.lib.stride_tricks import sliding_window_view
from numpy.typing import NDArray

XMAS_PAT = re.compile(r"(?=(XMAS))")


def diagonal_helper(array: NDArray[Any]) -> Iterator[NDArray[Any]]:
  for col_offset in range(array.shape[1]):
    yield array.diagonal(offset=col_offset)
  for row_offset in range(1, array.shape[0]):
    yield array.diagonal(offset=-row_offset)


def strings_to_ndarray(strings: list[str]) -> NDArray[np.str_]:
  array = np.array(strings, dtype="U").view("U1")
  n_rows, n_cols = len(strings), len(strings[0])
  array.resize((n_rows, n_cols))
  return array


def gen_lines(strings: list[str]) -> Iterator[str]:
  if not strings:
    return
  yield from strings
  array = strings_to_ndarray(strings)
  for col_idx in range(array.shape[1]):
    yield "".join(array[:, col_idx])
  yield from ("".join(diag) for diag in diagonal_helper(array))
  yield from ("".join(diag) for diag in diagonal_helper(np.fliplr(array)))


def part1(filename: str) -> int:
  with open(filename, "r") as infile:
    lines = [line.strip() for line in infile.readlines()]
  sum_ = 0
  for line in gen_lines(lines):
    sum_ += len(XMAS_PAT.findall(line))
    sum_ += len(XMAS_PAT.findall("".join(reversed(line))))
  return sum_


def part2(filename: str) -> int:
  with open(filename, "r") as infile:
    array = strings_to_ndarray([line.strip() for line in infile.readlines()])
  sum_ = 0
  for subarray in chain.from_iterable(sliding_window_view(array, (3, 3))):
    diag1 = "".join(subarray.diagonal())
    diag2 = "".join(np.fliplr(subarray).diagonal())
    if (diag1 == "SAM" or diag1 == "MAS") and (diag2 == "SAM" or diag2 == "MAS"):
      sum_ += 1
  return sum_


def main(filename: str):
  print(part1(filename))
  print(part2(filename))


if __name__ == "__main__":
  fire.Fire(main)
