import re
from itertools import chain

import fire

MUL_PAT = re.compile(r"mul\((\d{1,3}),(\d{1,3})\)")
INSTRUCTIONS_PAT: re.Pattern = re.compile(
  r"(?P<mul>mul\((\d{1,3}),(\d{1,3})\))" r"|" r"(?P<do>do\(\))" r"|" r"(?P<dont>don't\(\))"
)


def part1(filename: str):
  sum_ = 0
  with open(filename, "r") as infile:
    for line in infile:
      for match in MUL_PAT.finditer(line):
        left, right = match.groups()
        sum_ += int(left) * int(right)
  print(f"part 1: {sum_}")


def part2(filename: str):
  sum_ = 0
  enabled = True
  with open(filename, "r") as infile:
    for match in chain.from_iterable(INSTRUCTIONS_PAT.finditer(line) for line in infile):
      if match.group("dont") is not None:
        enabled = False
      if match.group("do") is not None:
        enabled = True
      if match.group("mul") is not None and enabled:
        _, left, right, *_ = match.groups()
        sum_ += int(left) * int(right)
  print(f"part 2: {sum_}")


def main(filename: str):
  part1(filename)
  part2(filename)


if __name__ == "__main__":
  fire.Fire(main)
