import re
from itertools import batched
from typing import Any

import fire
from sympy import Eq, Q, ask, ceiling, floor
from sympy.abc import x, y
from sympy.solvers.simplex import InfeasibleLPError, lpmin
from tqdm import tqdm

BUTTON_PAT = re.compile(r"Button (A|B): X\+(\d+), Y\+(\d+)")
PRIZE_PAT = re.compile(r"Prize: X=(\d+), Y=(\d+)")


def branch_and_bound(f, constr: tuple[Any, ...]):
  try:
    val, x_opt = lpmin(f, constr)
  except InfeasibleLPError:
    return float("inf"), {}
  if all(ask(Q.integer(xstar)) for xstar in x_opt.values()):
    return val, x_opt
  subproblems = []
  for x, xstar in [(y, ystar) for y, ystar in x_opt.items() if not ask(Q.integer(ystar))]:
    subproblems.append(branch_and_bound(f, (*constr, x >= ceiling(xstar))))
    subproblems.append(branch_and_bound(f, (*constr, x <= floor(xstar))))
  if not subproblems:
    return float("inf"), {}
  return min(subproblems, key=lambda p: p[0])


def solve(filename: str, is_part2: bool) -> int:
  sum_ = 0
  with open(filename, "r") as infile:
    for machine_desc in tqdm(list(batched(map(str.strip, infile), 4))):
      A_match = BUTTON_PAT.match(machine_desc[0])
      B_match = BUTTON_PAT.match(machine_desc[1])
      prize_match = PRIZE_PAT.match(machine_desc[2])
      if A_match is None or B_match is None or prize_match is None:
        raise IOError(f"failed to parse: {"\n".join(machine_desc)}")
      _, deltaX_A, deltaY_A = A_match.groups()
      _, deltaX_B, deltaY_B = B_match.groups()
      prize_X, prize_Y = prize_match.groups()
      a11, a12, a21, a22 = [int(x) for x in (deltaX_A, deltaX_B, deltaY_A, deltaY_B)]
      b1, b2 = int(prize_X), int(prize_Y)
      if is_part2:
        b1, b2 = b1 + 10000000000000, b2 + 10000000000000
      constr = (Eq(b1, a11 * x + a12 * y), Eq(b2, a21 * x + a22 * y), x >= 0, y >= 0)
      if not is_part2:
        constr = (*constr, x <= 100, y <= 100)
      val, x_opt = branch_and_bound(3 * x + y, constr)
      if val != float("inf"):
        sum_ += int(val)
  return sum_


def main(filename):
  print(solve(filename, is_part2=False))
  print(solve(filename, is_part2=True))


if __name__ == "__main__":
  fire.Fire(main)
