from __future__ import annotations

from dataclasses import dataclass, replace
from typing import cast

import fire


@dataclass(frozen=True)
class StorageBlock:
  id: int
  size: int
  defragged: bool = False

  @property
  def is_free(self) -> bool:
    return False

  def __str__(self) -> str:
    return f"{self.id}" * self.size

  def checksum(self, offset=0) -> int:
    sum_ = 0
    for mult in range(self.size):
      sum_ += (mult + offset) * self.id
    return sum_


@dataclass(frozen=True)
class FreeBlock:
  size: int

  @property
  def is_free(self) -> bool:
    return True

  def merge(self, other: FreeBlock) -> FreeBlock:
    return FreeBlock(size=self.size + other.size)

  def __str__(self) -> str:
    return f"." * self.size

  def checksum(self, offset=0) -> int:
    return 0


@dataclass(frozen=True)
class PackResult:
  packed: StorageBlock
  packed_free: FreeBlock | None
  remainder: StorageBlock | None
  remainder_free: FreeBlock | None


def pack(free: FreeBlock, storage: StorageBlock) -> PackResult:
  if free.size >= storage.size:
    diff = free.size - storage.size
    return PackResult(
      packed=StorageBlock(size=storage.size, id=storage.id, defragged=True),
      packed_free=FreeBlock(size=diff) if diff > 0 else None,
      remainder=None,
      remainder_free=FreeBlock(size=storage.size),
    )
  else:
    diff = storage.size - free.size
    return PackResult(
      packed=StorageBlock(size=free.size, id=storage.id, defragged=True),
      packed_free=None,
      remainder=StorageBlock(size=diff, id=storage.id),
      remainder_free=FreeBlock(size=free.size),
    )


@dataclass(frozen=True)
class DiskMap:
  blocks: tuple[StorageBlock | FreeBlock, ...]

  def __str__(self) -> str:
    return "".join(str(b) for b in self.blocks)

  def is_packed(self) -> bool:
    freed_space = False
    for block in self.blocks:
      if block.is_free:
        freed_space = True
      else:
        if freed_space:
          return False
    return True

  def first_free(self) -> tuple[int, FreeBlock | None]:
    for idx, block in enumerate(self.blocks):
      if block.is_free and type(block) == FreeBlock:
        return idx, block
    return len(self.blocks), None

  def last_storage(self) -> tuple[int, StorageBlock | None]:
    for idx in reversed(range(len(self.blocks))):
      block = self.blocks[idx]
      if not block.is_free and type(block) == StorageBlock and not block.defragged:
        return idx, block
    return -1, None

  def _apply_result(self, free_idx: int, storage_idx: int, result: PackResult) -> DiskMap:
    free_block = cast(FreeBlock, self.blocks[free_idx])
    storage_block = cast(StorageBlock, self.blocks[storage_idx])
    blocks = list(self.blocks)
    result = pack(free_block, storage_block)
    blocks[free_idx] = result.packed
    if result.packed_free is not None:
      neighbor = blocks[free_idx + 1]
      if neighbor.is_free and type(neighbor) == FreeBlock:
        blocks[free_idx + 1] = neighbor.merge(result.packed_free)
      else:
        blocks.insert(free_idx + 1, result.packed_free)
        storage_idx += 1
    if result.remainder is not None:
      blocks[storage_idx] = result.remainder
    elif result.remainder_free is not None:
      blocks[storage_idx] = result.remainder_free
    else:  # remainder and remainder_free are non
      del blocks[storage_idx]
    # merge right
    if storage_idx + 1 < len(blocks):
      left, right = blocks[storage_idx], blocks[storage_idx + 1]
      if type(left) == FreeBlock and type(right) == FreeBlock:
        blocks[storage_idx] = left.merge(right)
        del blocks[storage_idx + 1]
    # merge left
    left, right = blocks[storage_idx - 1], blocks[storage_idx]
    if type(left) == FreeBlock and type(right) == FreeBlock:
      blocks[storage_idx] = left.merge(right)
      del blocks[storage_idx - 1]
    return DiskMap(tuple(blocks))

  def pack(self) -> DiskMap:
    if self.is_packed():
      return self
    free_idx, free_block = self.first_free()
    storage_idx, storage_block = self.last_storage()
    if free_block is None or storage_block is None:
      return self
    result = pack(free_block, storage_block)
    return self._apply_result(free_idx, storage_idx, result)

  def is_defragged(self) -> bool:
    return all(b.defragged for b in self.blocks if type(b) == StorageBlock)

  def defrag(self) -> DiskMap:
    if self.is_defragged():
      return self
    storage_idx, storage_block = self.last_storage()
    if storage_block is None:
      return self
    for free_idx, block in enumerate(self.blocks):
      if storage_idx < free_idx:
        break
      if block.is_free and type(block) == FreeBlock and block.size >= storage_block.size:
        result = pack(block, storage_block)
        return self._apply_result(free_idx, storage_idx, result)
    blocks = list(self.blocks)
    blocks[storage_idx] = replace(blocks[storage_idx], defragged=True)
    return DiskMap(tuple(blocks))

  def checksum(self) -> int:
    sum_ = 0
    offset = 0
    for block in self.blocks:
      sum_ += block.checksum(offset)
      offset += block.size
    return sum_


def part2(diskmap: DiskMap) -> int:
  # print(diskmap)
  while not diskmap.is_defragged():
    diskmap = diskmap.defrag()
    # print(diskmap)
  return diskmap.checksum()


def part1(diskmap: DiskMap) -> int:
  # print(diskmap)
  while not diskmap.is_packed():
    diskmap = diskmap.pack()
    # print(diskmap)
  return diskmap.checksum()


def main(filename: str):
  blocks: list[StorageBlock | FreeBlock] = []
  with open(filename, "r") as infile:
    line = next(infile).strip()
    free = False
    id = 0
    for char in line:
      size = int(char)
      if size == 0:
        free = not free
        continue
      if free:
        blocks.append(FreeBlock(size=size))
      else:
        blocks.append(StorageBlock(id=id, size=size))
        id += 1
      free = not free
  diskmap = DiskMap(blocks=tuple(blocks))
  print(part1(diskmap))
  print()
  print(part2(diskmap))


if __name__ == "__main__":
  fire.Fire(main)
